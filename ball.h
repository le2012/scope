#include <Ecore.h>
#include <Ecore_Evas.h>
#include "scope.h"

static uint32_t *bp;
extern void *ball_pixels;
extern Evas_Object *ball_o;
extern volatile int ball_off;
extern pthread_mutex_t ball_mutex;
extern pthread_cond_t ball_cond;

static inline uint32_t color2uint32(int r, int g, int b, int a)
{ return (a << 24) | (r << 16) | (g << 8) | (b << 0); }

static inline void ioctlb(int a, int b, void *c)
{
	short *s = c;
	int x, y;
	pthread_mutex_lock(&ball_mutex);
	evas_object_image_size_get(ball_o, &x, &y);
	s[0] = y;
	s[1] = x;
}

static inline void printr(char *a) {
	bp = ball_pixels;
	evas_object_image_pixels_dirty_set(ball_o, EINA_TRUE);
	while (ball_off) {
		uint32_t *p = bp;
		int i, j, x, y;
		evas_object_image_size_get(ball_o, &x, &y);
		for (i,j = 0 ; j < y ; j+=(i=++i%x)==0)
			*p++ = (i/4 + j/4) & 1 ? color2uint32(WHITE) : color2uint32(RED);
		evas_object_image_pixels_dirty_set(ball_o, EINA_TRUE);
		pthread_cond_wait(&ball_cond, &ball_mutex);
	}
}

static inline void printp(char *a, int c)
{
	switch (c) {
		case 40:  *bp++ = color2uint32(BLACK); break;
		case 101: *bp++ = color2uint32(RED); break;
		case 107: *bp++ = color2uint32(WHITE); break;
		case 47:  *bp++ = color2uint32(GRAY); break;
		case 100: *bp++ = color2uint32(DARKGRAY); break;
	}
}
