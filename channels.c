#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include <limits.h>
#include <math.h>
#include <time.h>
#include <unistd.h>

#include "scope.h"

Channel chan[CHANNELS] = {
	{
		.name = "ADC1",
		.vdiv = 2,
		.invert = 0,
		.visible = 1,
	},
	{
		.name = "ADC2",
		.vdiv = 2,
		.invert = 0,
		.visible = 1,
	},
	{
		.name = "ADC3",
		.vdiv = 2,
		.invert = 0,
		.visible = 1,
		//.ac_ndc = 1,
	},
	{
		.name = "ADC4",
		.vdiv = 2.0,
		.invert = 0,
		.visible = 1,
	},
	{
		.name = "MODULE",
		.vdiv = 2.0,
		.invert = 0,
		.visible = 0,
	},
};
