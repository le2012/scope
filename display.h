void display_init(Evas *e);
extern void * display_thread(void *param);
extern int display_trigger_ypos_get(void);
extern int display_fps_get_reset(void);

static inline void display_update(void)
{
	extern volatile int display_refresh;
	display_refresh = 1;
}
