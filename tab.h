
Evas_Object * tab_add(Evas *evas);
void tab_title_set(Evas_Object *o, const char *fmt, ...);
void tab_info_set(Evas_Object *o, const char *fmt, ...);
void tab_title_color_set(Evas_Object *o, int r, int g, int b, int a);
