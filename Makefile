CC?=gcc

CFLAGS += -O2 -g

CFLAGS+=$(shell pkg-config --cflags evas ecore ecore-evas)
LDFLAGS+=$(shell pkg-config --libs evas ecore ecore-evas freetype2 eet zlib ecore-fb ecore-ipc ecore-input ecore-con tslib-0.0 ecore-input-evas) -ldl -lm -ljpeg

scope: main.o tab.o menus.o channels.o display.o curve.o acq.o record.o module.o ball.o
	$(CC) $^ -o $@ $(LDFLAGS) $(CFLAGS)

clean:
	rm *.o scope *.so tags
