#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "scope.h"
#include "8x8font.h"

static uint8_t scope[251][201];

const int width = 320;
const int height = 240;
uint16_t *pixels;


static const uint16_t grid_bg = BLACK;
static const uint16_t grid_fg = DARKGRAY;

#define MAX_CHANNELS 7
static int old[MAX_CHANNELS][500] = { 0 };

static uint16_t colors[8] = {
	L_RED,
	L_YELLOW,
	L_MAGENTA,
	L_CYAN,
	L_GREEN,
	WHITE,
	L_BLUE,
	DARKGRAY,	// grid foreground
};

#if 0
static uint16_t grid_color(int x, int y)
{
	uint16_t color = grid_bg;
	if (x == 10 || x == 260 || y == 20 || y == 220) {
		color = grid_fg;
	} else if (((x - 10) % 5) == 0 &&
			((y - 20) % 5) == 0 &&
			(((x - 10) % 25) == 0 || ((y - 20) % 25) == 0)) {
		color = grid_fg;
	}
	return color;
}
#endif

static void scope_pixel(int x, int y, uint16_t color)
{
	pixels[10 + x + (220 - y) * 320] = color;
}

/*
 * +---------+
 * |    |    |
 * |    |    |
 * +----+----+
 * |    |    |
 * |    |    |
 * +----+----+
 *
 *
 *
 */
void display_grid(void)
{
	int x, y;

	memset(scope, 0, sizeof scope);

	for (y = 21 ; y < 220 ; ++y) {
		for (x = 11 ; x < 260 ; ++x) {
			pixels[x + y * 320] = BLACK;
		}
	}

	for (x = 0 ; x <= 250 ; ++x) {
		pixels[10 + x + 20 * 320] = DARKGRAY;
		pixels[10 + x + 220 * 320] = DARKGRAY;
		if ((x % 25) == 0) {
			for (y = 0 ; y <= 200 ; y += 5) {
				scope_pixel(x, y, DARKGRAY);
				scope[x][y] = 1 << 7;
			}
		}
	}
	for (y = 0 ; y <= 200 ; ++y) {
		pixels[10 + (20 + y) * 320] = DARKGRAY;
		pixels[260 + (20 + y) * 320] = DARKGRAY;
		if ((y % 25) == 0) {
			for (x = 0 ; x <= 250 ; x += 5) {
				scope_pixel(x, y, DARKGRAY);
				scope[x][y] = 1 << 7;
			}
		}
	}
}

static void scope_update(int x, int y)
{
	uint8_t v = scope[x][y];
	uint16_t color;

	if (v == 0) {
		color = BLACK;
	} else {
		uint16_t *p = colors;
		int i;
		while (v) {
			if (v & 1)
				break;
			p++;
			v >>= 1;
		}
		color = *p;

	}

	scope_pixel(x, y, color);
}

static void scope_add(int x, int y, int channel)
{
	if (y >= 1 && y < 200) {
		scope[x][y] |= 1 << channel;
		scope_update(x, y);
	}
}

static void scope_del(int x, int y, int channel)
{
	if (y >= 1 && y < 200) {
		scope[x][y] &= ~(1 << channel);
		scope_update(x, y);
	}
}

void channel_update(int channel, int *val)
{
	int x, y;
	int ya, yb;

	for (x = 0 ; x < 249 ; ++x) {
		ya = old[channel][2*x];
		yb = old[channel][2*x+1];
		scope_del(x+1, ya, channel);
		for (y = ya ; y != yb ; ya < yb ?  ++y : --y) {
			scope_del(x+1, y, channel);
		}
	}

	memcpy(old[channel], val, 500 * sizeof *val);

	for (x = 0 ; x < 249 ; ++x) {
		ya = val[2*x];
		yb = val[2*x+1];
		scope_add(x+1, ya, channel);
		for (y = ya ; y != yb ; ya < yb ?  ++y : --y) {
			scope_add(x+1, y, channel);
		}
	}
}

void hline(int x1, int x2, int y, uint16_t color)
{
	for ( ; x1 <= x2 ; ++x1) {
		pixels[x1 + 320 * y] = color;
	}
}

void rectangle(int x1, int y1, int x2, int y2, uint16_t color)
{
	for ( ; y1 <= y2 ; ++y1) {
		hline(x1, x2, y1, color);
	}
}

void onglet(int x1, int y1, int x2, int y2, uint16_t color)
{
	hline(x1+3, x2, y1++, color);
	hline(x1+1, x2, y1++, color);
	hline(x1+1, x2, y1++, color);
	hline(x1+3, x2, y2--, color);
	hline(x1+1, x2, y2--, color);
	hline(x1+1, x2, y2--, color);
	rectangle(x1, y1, x2, y2, color);
}

void display_char(int x, int y, char c, uint16_t color)
{
	int i, j;

	if (c < 0) return;

	for (i = 0 ; i < 8 ; ++i) {
		uint8_t *p = &font8x8[8 * c + i * 1024];
		for (j = 0 ; j < 8 ; ++j) {
			if (p[j])
				pixels[x + j + (y + i) * 320] = color;
		}
	}
}

void display_str(int x, int y, char *s, uint16_t color, int e)
{
	int c, i;
	while ((c = *s++)) {
		for (i = 0 ; i <= e ; ++i)
			display_char(x+i, y, c, color);
		x += 6 + e;
	}
}

static void display_ypos(int ypos, uint16_t color)
{
	hline(5, 5, 220 - ypos - 4, color);
	hline(5, 6, 220 - ypos - 3, color);
	hline(5, 7, 220 - ypos - 2, color);
	hline(5, 8, 220 - ypos - 1, color);
	hline(5, 9, 220 - ypos, color);
	hline(5, 8, 220 - ypos + 1, color);
	hline(5, 7, 220 - ypos + 2, color);
	hline(5, 6, 220 - ypos + 3, color);
	hline(5, 5, 220 - ypos + 4, color);
}

static void display_xpos(int xpos, uint16_t color)
{
	hline(10 + xpos, 10 + xpos, 221, color);
	hline(10 + xpos - 1, 10 + xpos + 1, 222, color);
	hline(10 + xpos - 2, 10 + xpos + 2, 223, color);
	hline(10 + xpos - 3, 10 + xpos + 3, 224, color);
	hline(10 + xpos - 4, 10 + xpos + 4, 225, color);
}

static Action action_list[200];
static int nb_actions;

static void menu_onglet(int item, uint16_t color, char *str1, char *str2)
{
	int y = item >= 0 ? 20 + item * 24 : 224 + item * 24;

	onglet(264, y, 269, y + 20, color);
	rectangle(270, y, 319, y + 20, GRAY);
	display_str(270, y + 2, str1, BLACK, 1);
	display_str(270, y + 11, str2, BLACK, 0);
}

void channel_config_menu(int channel)
{
	char str[9];
	uint16_t color = colors[channel];
	Channel *ch = &chan[channel];

	rectangle(264, 20, 319, 220, BLACK);

	menu_onglet(0, color, "Source", ch->src->name);

	if (ch->vdiv < 0.1) {
		sprintf(str, "%d mV/d", (int) (ch->vdiv * 1000));
	} else {
		sprintf(str, "%1.1f V/d", ch->vdiv);
	}
	menu_onglet(1, color, "Calib", str);

	menu_onglet(2, color, "Invert", ch->invert ? "YES" : "NO");

	sprintf(str, "% d", ch->ypos - 100);
	menu_onglet(3, color, "Y-POS", str);

	menu_onglet(-1, DARKGRAY, "RETURN", "");
}

void channels_menu(void)
{
	char str[9];

	rectangle(264, 20, 319, 220, BLACK);

	for (int i = 0; i < CHANNELS ; i++) {
		Channel *ch = &chan[i];

		onglet(264, 20 + i*24, 269, 20+20 + i*24, colors[i]);
		rectangle(270,20 + i*24, 319, 20+20 + i*24, GRAY);

		if (ch->src) {
			if (ch->vdiv < 0.1) {
				sprintf(str, "%d mV/d", (int) (ch->vdiv * 1000));
			} else {
				sprintf(str, "%1.1f V/d", ch->vdiv);
			}

			menu_onglet(i, colors[i], ch->src->name, str);

			display_ypos(ch->ypos, colors[i]);
		} else {
			menu_onglet(i, colors[i], "Off", "");
		}

	}

	if (tdiv < 0.1) {
		sprintf(str, "%d ms/d", (int) (tdiv * 1000));
	} else {
		sprintf(str, "%1.1f s/d", tdiv);
	}
	menu_onglet(-2, DARKGRAY, "Time", str);

	sprintf(str, "% 1.3f V", trigger_val);
	menu_onglet(-1, colors[trigger_chan], trigger_sense ? "Trig \1" : "Trig \2", str);

	display_xpos(trigger_xpos, colors[trigger_chan]);
}


void ui_mouse(int x, int y, int down)
{
	pixels[x + y * 320] = WHITE;
}

