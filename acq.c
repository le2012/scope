#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <pthread.h>
#include <math.h>
#include <time.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <Eina.h>

#include "scope.h"
#include "acq.h"

#define IOCTL_ADC_WPOS _IOR('A', 1, unsigned long)
#define IOCTL_ADC_FREQ _IOW('A', 2, unsigned long)

float min_voltage = 0.0;
float max_voltage = 3.3;
int bits_per_sample = 10;

static Eina_List *counters;
static pthread_mutex_t counters_mutex = PTHREAD_MUTEX_INITIALIZER;

void
acq_add_countdown(acq_countdown *counter)
{
	pthread_mutex_lock(&counters_mutex);
	counters = eina_list_append(counters, counter);
	if (eina_error_get()) abort();
	pthread_mutex_unlock(&counters_mutex);
}

static void
check_counters(void)
{
	acq_countdown *counter;
	Eina_List *l, *l_next;

	pthread_mutex_lock(&counters_mutex);
	EINA_LIST_FOREACH_SAFE(counters, l, l_next, counter) {
		if (counter->target <= acq_wpos &&
		    pthread_mutex_trylock(&counter->mutex) == 0) {
			counters = eina_list_remove_list(counters, l);
			pthread_cond_signal(&counter->cond);
			pthread_mutex_unlock(&counter->mutex);
		}
	}
	pthread_mutex_unlock(&counters_mutex);
}

/*
 * Reserve space to record 2s of ADC samples
 */
volatile uint16_t *acq_samples;
volatile uint64_t acq_wpos;
volatile int acq_freq = 40000;

void
acq_set_freq(int freq)
{
	int fd;
	fd = open("/dev/adc", O_RDONLY);
	if (fd < 0) {
		perror("open(\"/dev/adc\")");
		return;
	}
	ioctl(fd, IOCTL_ADC_FREQ, (unsigned long) freq);
	close(fd);
	acq_freq = freq;
}

void *
acquisition_thread(void *param)
{
	int a[ACQ_CHANNELS];
	int i;
	//float t0 = 0.0;
	int fd;
	unsigned long wpos, last;
	struct sched_param sp;

#ifdef USE_PRIO
	sp.sched_priority = ACQ_THREAD_PRIO;
	pthread_setschedparam(pthread_self(), SCHED_RR, &sp);
#endif

	acq_set_freq(acq_freq);

	fd = open("/dev/adc", O_RDONLY);
	if (fd < 0) {
		perror("open(\"/dev/adc\")");
		abort();
	}

	acq_samples = mmap(NULL, ACQ_BUFFER_SIZE, PROT_READ, MAP_SHARED, fd, 0);
	if (acq_samples == MAP_FAILED) {
		perror("mmap(\"/dev/adc\")");
		abort();
	}

	for (i = 0 ; i < ACQ_CHANNELS ; i++) {
		a[i] = rand() / 2;
	}

#if 0
	struct timespec ts;
	uint32_t ms0, ms1;
	clock_gettime(CLOCK_MONOTONIC, &ts);
	ms0 = ts.tv_sec * 1000 + ts.tv_nsec / 1000000;
#endif
	ioctl(fd, IOCTL_ADC_WPOS, &wpos);
	wpos &= ~(ACQ_CHANNELS * 2 - 1);
	last = wpos / ACQ_CHANNELS / 2;
	acq_wpos = last;

	while (1) {
		int samples, n;

		/* relax cpu */
		usleep(10000);

		ioctl(fd, IOCTL_ADC_WPOS, &wpos);
		wpos = wpos / ACQ_CHANNELS / 2;
		samples = wpos - last;
		if (samples < 0) samples += ACQ_SAMPLES;
		last = wpos;
		acq_wpos += samples;
#if 0
		clock_gettime(CLOCK_MONOTONIC, &ts);
		ms1 = ts.tv_sec * 1000 + ts.tv_nsec / 1000000;

		samples = (ms1 - ms0) * acq_freq / 1000;
		ms0 = ms1;
		wpos = acq_wpos % ACQ_SAMPLES;
		for (n = 0 ; n < samples ; ++n) {
			volatile uint16_t *v = acq_samples[ACQ_CHANNELS * wpos];
			int i, m;
			float t = t0 + (float) n / acq_freq;

			v[0] = 127 * (1 + cos(a[0] + t * 250.0 * 3.14159265));
			v[1] = ((a[1] + (int)(t*4000)) & 0x3f) < 0x20 ? 0 : 256;
			v[2] = ((a[2] + (int)(t*4000)) & 0x3f) * 3;
			m = (a[3] + (int)(t*80000)) & 0x3f;
			v[3] = (m > 32 ? (m - 32) : 32 - m) * 8;

			for (i = 0 ; i < ACQ_CHANNELS ; i++) {
				//v[i] += ((float) rand() / RAND_MAX - 0.5) / 30;
			}

			wpos = (wpos + 1) % ACQ_SAMPLES;
		}
#endif

		//t0 += (float) samples / acq_freq;
		check_counters();
	}

	return NULL;
}
