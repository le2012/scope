#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <pthread.h>
#include <math.h>
#include <time.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>
#include <dlfcn.h>

#include <Ecore.h>
#include <Ecore_Evas.h>

#include "scope.h"
#include "acq.h"
#include "module.h"

#ifndef MODULE_DIR
#define MODULE_DIR "/mnt"
#endif

struct plugin *plugin;
Eina_List *plugin_list;
int plugin_args[ACQ_CHANNELS] = { 0, 1, 2, 3 };

t_plugin_private *display_ctxt, *record_ctxt;

static pthread_mutex_t module_mutex = PTHREAD_MUTEX_INITIALIZER;

void
module_init(void)
{
	char filename[PATH_MAX];
	DIR *dirp;
	struct dirent *entry;
	Eina_List *l = NULL;
	void *handle;
	struct plugin *p;

	dirp = opendir(MODULE_DIR);
	if (dirp == NULL) {
		perror("opendir");
		return;
	}

	while ((entry = readdir(dirp))) {
		int len = strlen(entry->d_name);
		if (len < 4) continue;
		if (strcmp(".so", entry->d_name + len - 3)) continue;
		sprintf(filename, MODULE_DIR "/%s", entry->d_name);
		handle = dlopen(filename, RTLD_LAZY);
		if (handle == NULL) continue;
		p = dlsym(handle, "plugin");
		if (p == NULL) { dlclose(handle); continue; }
		printf("name %s, author %s\n", p->name, p->author);
		plugin_list = eina_list_append(plugin_list, p);
	}

	closedir(dirp);
}

int
plugin_nb_args_get(void)
{
	return plugin->plugin_ops.v1->nb_args;
}

void
module_select(struct plugin *p)
{
	pthread_mutex_lock(&module_mutex);
	if (plugin) {
		plugin->plugin_ops.v1->destroy(display_ctxt);
		plugin->plugin_ops.v1->destroy(record_ctxt);
	}
	if (p) {
		display_ctxt = p->plugin_ops.v1->init();
		record_ctxt = p->plugin_ops.v1->init();
	}
	plugin = p;
	pthread_mutex_unlock(&module_mutex);
}

void
module_get(void)
{
	pthread_mutex_lock(&module_mutex);
}

void
module_put(void)
{
	pthread_mutex_unlock(&module_mutex);
}
