Evas_Object * curve_add(Evas *evas);
extern void curve_update(Evas_Object *o, int y_min[], int y_max[]);
extern unsigned int curve_distance_get(Evas_Object *o, Evas_Coord x, Evas_Coord y);
extern void curve_mousedown(Evas_Object *o, Evas_Coord x, Evas_Coord y);
extern int curve_ypos_get(Evas_Object *o);
extern void curve_ypos_set(Evas_Object *o, int ypos);

