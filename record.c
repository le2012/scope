#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <pthread.h>
#include <math.h>
#include <time.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <Ecore.h>
#include <Ecore_Evas.h>

#include "scope.h"
#include "acq.h"
#include "record.h"
#include "module.h"

#define VERSION_MAJOR	1
#define VERSION_MINOR	0

#ifndef RECORD_DIR
#define RECORD_DIR "/mnt"
#endif

//#define RECORD_MODULE

static void *record_thread(void *param);
static acq_countdown record_countdown;

static volatile uint64_t stop_at;
volatile int recording, mod_recording;

static Evas_Object *record_date, *record_rect;
static Evas_Object *acq_rect, *rec_rect;
static Ecore_Timer *record_timer;
static pthread_t rec_thread, mod_thread, flush_th;
static uint64_t wpos64_start;
static volatile uint64_t wpos64, wpos64_r;
static char date[PATH_MAX];
static int recNumber;

#define RECORD_SAMPLES (4<<20)	// 8MB per track (max 40 MB !)
static uint16_t rec_samples[CHANNELS][RECORD_SAMPLES];
static volatile uint64_t rec_wpos64[CHANNELS];
static int rec_pcm[CHANNELS];
static volatile uint64_t rec_added, rec_done;

static Eina_Bool
_on_record_timer(void *data)
{
	char str[32];
	uint64_t at = stop_at > acq_wpos ? acq_wpos : stop_at;

	if (stop_at != -1ULL)
		evas_object_color_set(record_rect, DARKGRAY);

	int dsec = (at - wpos64_start) * 10 / acq_freq;
	sprintf(str, "#%03d %02d:%02d.%01d", recNumber, dsec / 600, (dsec / 10) % 60, dsec % 10);
	evas_object_text_text_set(record_date, str);

	int count = at - wpos64;
	int p = count * 50 / ACQ_SAMPLES;
	if (p > 50) p = 50;
	int c = count * 255 / ACQ_SAMPLES;
	if (c > 255) c = 255;
	evas_object_resize(acq_rect, p + 1, 7);
	evas_object_color_set(acq_rect, c, 255 - c, 0, 255);

	count = (int) (rec_added - rec_done);
	count /= wpos64_r == -1ULL ? 4 : 5;
	p = count * 50 / RECORD_SAMPLES;
	if (p > 50) p = 50;
	c = count * 255 / RECORD_SAMPLES;
	if (c > 255) c = 255;
	evas_object_resize(rec_rect, p + 1, 7);
	evas_object_color_set(rec_rect, c, 255 - c, 0, 255);

	return EINA_TRUE;
}

static void
_record_stop(void *param)
{
	char prefix[PATH_MAX];
	char str[32];

	ecore_timer_del(record_timer);

	do {
		sprintf(prefix, RECORD_DIR "/record_%03d", ++recNumber);
	} while(access(prefix, R_OK) == 0);

	sprintf(str, " Record #%03d", recNumber);
	evas_object_text_text_set(record_date, str);
	evas_object_color_set(record_rect, RED);
	evas_object_resize(acq_rect, 0, 7);
	evas_object_resize(rec_rect, 0, 7);
}

static void
_on_mousedown(void *data, Evas *e, Evas_Object *o, void *einfo)
{
	time_t t;
	struct tm tm;

	if (recording) {
		evas_object_color_set(record_rect, DARKGRAY);
		if (stop_at == -1ULL)
			stop_at = acq_wpos;
		return;
	}

	t = time(NULL);
	gmtime_r(&t, &tm);
	sprintf(date, "%04d-%02d-%02dT%02d:%02d:%02dZ",
		tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday,
		tm.tm_hour, tm.tm_min, tm.tm_sec);

	stop_at = -1ULL;
	wpos64_start = acq_wpos;
	wpos64 = wpos64_start;
#ifdef RECORD_MODULE
	wpos64_r = (plugin && plugin->plugin_ops.v1->type == PLUGIN_RET_SIGNAL) ? wpos64_start : -1ULL;
	mod_recording = 1;
#else
	wpos64_r = -1ULL;
#endif
	rec_added = rec_done = 0;
	evas_object_color_set(record_rect, L_RED);
	recording = 1;

	record_timer = ecore_timer_add(.1, _on_record_timer, NULL);

	if (pthread_create(&rec_thread, NULL, record_thread, NULL) != 0) {
		perror("Cannot start record thread");
		ecore_evas_shutdown();
		exit(EXIT_FAILURE);
	}
}


void
record_init(Evas *e)
{
	Evas_Object *o;
	char prefix[PATH_MAX];
	char str[32];
	const int x = 175;

	do {
		sprintf(prefix, RECORD_DIR "/record_%03d", ++recNumber);
	} while(access(prefix, R_OK) == 0);

	o = evas_object_rectangle_add(e);
	evas_object_color_set(o, RED);
	evas_object_move(o, x, 0);
	evas_object_resize(o, 81, 32);
	evas_object_event_callback_add(o, EVAS_CALLBACK_MOUSE_DOWN, _on_mousedown, NULL);
	evas_object_show(o);
	record_rect = o;

	o = evas_object_text_add(e);
	evas_object_color_set(o, WHITE);
	sprintf(str, " Record #%03d", recNumber);
	evas_object_text_text_set(o, str);
	evas_object_text_font_source_set(o, FONT_EET);
	evas_object_text_font_set(o, FONT ":style=Bold", 10);
	evas_object_move(o, x+2, 1);
	evas_object_anti_alias_set(o, EINA_FALSE);
	evas_object_pass_events_set(o, EINA_TRUE);
	evas_object_show(o);
	record_date = o;

	o = evas_object_text_add(e);
	evas_object_color_set(o, WHITE);
	evas_object_text_text_set(o, "ADC");
	evas_object_text_font_source_set(o, FONT_EET);
	evas_object_text_font_set(o, FONT, 10);
	evas_object_move(o, x+2, 11);
	evas_object_anti_alias_set(o, EINA_FALSE);
	evas_object_pass_events_set(o, EINA_TRUE);
	evas_object_show(o);

	o = evas_object_text_add(e);
	evas_object_color_set(o, WHITE);
	evas_object_text_text_set(o, "REC");
	evas_object_text_font_source_set(o, FONT_EET);
	evas_object_text_font_set(o, FONT, 10);
	evas_object_move(o, x+2, 21);
	evas_object_anti_alias_set(o, EINA_FALSE);
	evas_object_pass_events_set(o, EINA_TRUE);
	evas_object_show(o);

	o = evas_object_rectangle_add(e);
	evas_object_color_set(o, GRAY);
	evas_object_move(o, x+26, 12);
	evas_object_resize(o, 53, 9);
	evas_object_pass_events_set(o, EINA_TRUE);
	evas_object_show(o);

	o = evas_object_rectangle_add(e);
	evas_object_color_set(o, GRAY);
	evas_object_move(o, x+26, 22);
	evas_object_resize(o, 53, 9);
	evas_object_pass_events_set(o, EINA_TRUE);
	evas_object_show(o);

	o = evas_object_rectangle_add(e);
	evas_object_color_set(o, BLACK);
	evas_object_move(o, x+27, 13);
	evas_object_resize(o, 51, 7);
	evas_object_pass_events_set(o, EINA_TRUE);
	evas_object_show(o);

	o = evas_object_rectangle_add(e);
	evas_object_color_set(o, BLACK);
	evas_object_move(o, x+27, 23);
	evas_object_resize(o, 51, 7);
	evas_object_pass_events_set(o, EINA_TRUE);
	evas_object_show(o);

	o = evas_object_rectangle_add(e);
	evas_object_color_set(o, L_GREEN);
	evas_object_move(o, x+27, 13);
	evas_object_resize(o, 0, 7);
	evas_object_pass_events_set(o, EINA_TRUE);
	evas_object_show(o);
	acq_rect = o;

	o = evas_object_rectangle_add(e);
	evas_object_color_set(o, L_GREEN);
	evas_object_move(o, x+27, 23);
	evas_object_resize(o, 0, 7);
	evas_object_pass_events_set(o, EINA_TRUE);
	evas_object_show(o);
	rec_rect = o;

}

static void *
flush_thread(void *param)
{
	struct timespec ts;
	uint32_t ms, ms0;
	int i, stop = 0;
	uint64_t rec_rpos64[CHANNELS] = { 0 };
	struct sched_param sp;
	int done;

#ifdef USE_PRIO
	/* Set record thread realtime priority */
	sp.sched_priority = FLUSH_THREAD_PRIO;
	pthread_setschedparam(pthread_self(), SCHED_RR, &sp);
#endif

	while (!stop) {
		done = 0;

		stop = wpos64 >= stop_at && wpos64_r >= stop_at;
		for (i = 0 ; i < CHANNELS ; ++i) {
			uint32_t wpos, rpos, count;

			count = rec_wpos64[i] - rec_rpos64[i];
			if (!stop && count < RECORD_SAMPLES / 8)
				continue;

			if (count >= RECORD_SAMPLES) {
				printf("** flush underrun !!! **\n");
				stop_at = wpos64;
				memset(&rec_samples[i][0], 0, RECORD_SAMPLES * sizeof rec_samples[0][0]);
			}

			wpos = rec_wpos64[i] % RECORD_SAMPLES;
			rpos = rec_rpos64[i] % RECORD_SAMPLES;
			if (rpos > wpos) {
				count = RECORD_SAMPLES - rpos;
				stop = 0;
			}

			clock_gettime(CLOCK_MONOTONIC, &ts);
			ms0 = ts.tv_sec * 1000UL + ts.tv_nsec / 1000000;

			write(rec_pcm[i], &rec_samples[i][rpos], count * sizeof rec_samples[0][0]);

			clock_gettime(CLOCK_MONOTONIC, &ts);
			ms = ts.tv_sec * 1000UL + ts.tv_nsec / 1000000;
			if (ms > ms0)
				printf("record: ch %d, %u KB/s\n", i, count * sizeof rec_samples[0][0] / (ms - ms0));

			rec_rpos64[i] += count;
			rec_done += count;
			done = 1;
		}

		if (!done)
			usleep(10000);
	}

	return NULL;
}

static void *
record_module_thread(void *param)
{
	int samples = acq_freq / 20;
	uint16_t *volts[CHANNELS];
	struct plugin_signal arg[CHANNELS];
	int i, wpos, rec_wpos, underrun = 0;

	struct plugin_signal_settings settings = {
		.bits_per_sample = bits_per_sample,
		.min_voltage = min_voltage,
		.max_voltage = max_voltage,
		.period = 1.0 / acq_freq,
	};
	plugin->plugin_ops.v1->reset(record_ctxt, &settings);

	for (i = 0 ; i < CHANNELS ; ++i) {
		volts[i] = malloc(samples * sizeof *volts[0]);
		arg[i].volts = volts[i];
		arg[i].start = 0;
	}

	acq_countdown countdown;
	pthread_mutex_init(&countdown.mutex, NULL);
	pthread_cond_init(&countdown.cond, NULL);
	pthread_mutex_lock(&countdown.mutex);

	while (wpos64_r < stop_at) {
		uint64_t target = wpos64_r + samples;
		int count, n;

		if (target > acq_wpos) {
			underrun = 0;
			countdown.target = target;
			acq_add_countdown(&countdown);
			pthread_cond_wait(&countdown.cond, &countdown.mutex);
		}

		count = acq_wpos - wpos64_r;
		if (count >= ACQ_SAMPLES) {
			printf("** module record underrun **\n");
			underrun = 1;
		}
		if (count > samples)
			count = samples;
		if (wpos64_r + count > stop_at)
			count = stop_at - wpos64_r;
		wpos = wpos64_r % ACQ_SAMPLES;
		rec_wpos = rec_wpos64[ACQ_CHANNELS] % RECORD_SAMPLES;

		if (!underrun) {
			for (i = 0 ; i < plugin_nb_args_get() ; ++i) {
				arg[i].nb_elts = count;
				arg[i].end = count;
				for (n = 0 ; n < count ; ++n) {
					volts[i][n] = acq_samples[ACQ_CHANNELS * ((wpos + n) % ACQ_SAMPLES) + plugin_args[i]];
				}
			}

			plugin->plugin_ops.v1->process.ret_signal(record_ctxt, arg, &arg[ACQ_CHANNELS]);

			for (n = 0 ; n < count ; ++n) {
				uint16_t v = volts[ACQ_CHANNELS][n];
				
				rec_samples[ACQ_CHANNELS][(rec_wpos + n) % RECORD_SAMPLES] = (1 << 15) + v << (15 - bits_per_sample);
			}
		} else {
			for (n = 0 ; n < count ; ++n)
				rec_samples[ACQ_CHANNELS][(rec_wpos + n) % RECORD_SAMPLES] = 0;
		}

		wpos64_r += count;
		rec_wpos64[ACQ_CHANNELS] += count;
		rec_added += count;
	}

	for (i = 0 ; i < CHANNELS ; ++i)
		free(volts[i]);

	return NULL;
}

static void *
record_thread(void *param)
{
	char prefix[PATH_MAX];
	char filename[PATH_MAX];
	FILE *cfg;
	int i, wpos;
#ifdef RECORD_MODULE
	int mod = plugin && plugin->plugin_ops.v1->type == PLUGIN_RET_SIGNAL;
#else
	int mod = 0;
#endif
	char *p = prefix;
	struct sched_param sp;

	sprintf(prefix, RECORD_DIR "/record_%03d", recNumber);
	if (mkdir(prefix, 0777) == -1) {
		perror("mkdir");
		goto out;
	}

	for (i = 0 ; i < CHANNELS ; ++i)
		rec_wpos64[i] = 0;

	for (i = 0 ; i < (mod ? CHANNELS : ACQ_CHANNELS) ; ++i) {
		sprintf(filename, "%s/chan%d.pcm", prefix, i + 1);
		rec_pcm[i] = open(filename, O_WRONLY|O_CREAT|O_TRUNC|O_SYNC, 0666);
		if (rec_pcm[i] == -1) {
			fprintf(stderr, "open(%s): %s\n", filename, strerror(errno));
			while (i--)
				close(rec_pcm[i]);
			goto out;
		}
	}

	if (mod) {
		if (pthread_create(&mod_thread, NULL, record_module_thread, NULL) != 0) {
			perror("Cannot start module record thread");
		}
	}

	if (pthread_create(&flush_th, NULL, flush_thread, NULL) != 0) {
		perror("Cannot start flush thread");
	}

#ifdef USE_PRIO
	/* Set record thread realtime priority */
	sp.sched_priority = REC_THREAD_PRIO;
	pthread_setschedparam(pthread_self(), SCHED_RR, &sp);
#endif

	pthread_mutex_init(&record_countdown.mutex, NULL);
	pthread_cond_init(&record_countdown.cond, NULL);

	pthread_mutex_lock(&record_countdown.mutex);

	int samples = acq_freq / 2;
	samples = ACQ_SAMPLES * 3 / 4;
	uint32_t us = 1000000ULL * ACQ_SAMPLES / acq_freq / 10;

	while (wpos64 < stop_at) {
		uint64_t target = wpos64 + samples;
		int count, n;

		//usleep(us);
		if (target > acq_wpos) {
			record_countdown.target = target;
			acq_add_countdown(&record_countdown);
			pthread_cond_wait(&record_countdown.cond, &record_countdown.mutex);
		}

		count = acq_wpos - wpos64;
		if (count >= ACQ_SAMPLES) {
			printf("** record underrun **\n");
			stop_at = wpos64;
			break;
		}
		if (count > samples)
			count = samples;
		if (wpos64 + count > stop_at)
			count = stop_at - wpos64;

		wpos = wpos64 % ACQ_SAMPLES;
		for (i = 0 ; i < ACQ_CHANNELS ; ++i) {
			uint32_t rec_wpos = rec_wpos64[i] % RECORD_SAMPLES;
			for (n = 0 ; n < count ; ++n) {
				uint16_t v = acq_samples[ACQ_CHANNELS * ((wpos + n) % ACQ_SAMPLES) + i];
				rec_samples[i][(rec_wpos + n) % RECORD_SAMPLES] = (1 << 15) + v << (15 - bits_per_sample);
			}
			rec_wpos64[i] += count;
			rec_added += count;
		}

		wpos64 += count;
	}

	if (mod) {
		pthread_join(mod_thread, NULL);
		mod_thread = (pthread_t) 0;
	}

	pthread_join(flush_th, NULL);
	flush_th = (pthread_t) 0;

	for (i = 0 ; i < (mod ? CHANNELS : ACQ_CHANNELS) ; ++i)
		close(rec_pcm[i]);

	wpos64 -= wpos64_start;
	sprintf(filename, "%s/record.cfg", prefix);
	cfg = fopen(filename, "w");
	if (cfg == NULL) {
		fprintf(stderr, "fopen(%s): %s\n", filename, strerror(errno));
		goto out;
	}
	fprintf(cfg, "version=%d.%d;\n", VERSION_MAJOR, VERSION_MINOR);
	fprintf(cfg, "date=\"%s\";\n\n", date);
	fprintf(cfg, "sample:\n{\n");
	fprintf(cfg, "\tcount=%lld;\n", wpos64);
	fprintf(cfg, "\t#freq=%d;\n", acq_freq);
	fprintf(cfg, "\tduration=%f;\n", ((float) wpos64) / acq_freq);
	fprintf(cfg, "};\n");
	for (i = 0 ; i < (mod ? CHANNELS : ACQ_CHANNELS) ; ++i) {
		fprintf(cfg, "\nchannel:\n{\n");
		if (i < ACQ_CHANNELS)
			fprintf(cfg, "\tname=\"ADC%d\";\n", i + 1);
		else
			fprintf(cfg, "\tname=\"MODULE\";\n");
		fprintf(cfg, "\tcoupling=DC;\n");
		fprintf(cfg, "\tfile:\n\t{\n");
		fprintf(cfg, "\t\tfilename=\"chan%d.pcm\";\n", i + 1);
		fprintf(cfg, "\t\tformat=16_LE;\n");
		fprintf(cfg, "\t\tmin_voltage=%.1f;\n", 0.0);
		fprintf(cfg, "\t\tmax_voltage=%.1f;\n", 3.3);
		fprintf(cfg, "\t};\n");
		fprintf(cfg, "};\n");
	}
	fclose(cfg);

out:
	ecore_main_loop_thread_safe_call_async(_record_stop, NULL);
	recording = 0;
	mod_recording = 0;

	return NULL;
}
