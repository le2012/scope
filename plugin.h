/**
 * @file   plugin.h
 * @author Albin Kauffmann <albin.kauffmann@openwide.fr>
 *
 * @brief  API for the oscilloscope plugins
 *
 * This API defines the interface between the oscilloscope and the plugins that
 * give calculation functions.
 * This interface is inspired from the code provided by Yargil. Thank you Yargil!
 */

#ifndef _PLUGIN_H_
# define _PLUGIN_H_

# include <stdint.h>

# define PLUGIN_API_VERSION_MAJOR 0x01
# define PLUGIN_API_VERSION_MINOR 0x01
# define PLUGIN_API_VERSION ((PLUGIN_API_VERSION_MAJOR << 8) |\
                             PLUGIN_API_VERSION_MINOR)

/**
 * Structure representing a signal
 *
 * This can be a signal comming from the ADC or the result of a computation.
 * This structure is used to pass data to plugins in real-time of from a
 * previously-recorded dataset.
 */
struct plugin_signal
{
	uint16_t      *volts;         /**< array of converted voltages */
	unsigned int  nb_elts;        /**< number of elements in the volts array */
	unsigned int  start;          /**< iterator to the start of data to compute */
	unsigned int  end;            /**< iterator to the end of data to compute */
};

/**
 * Structure giving signal settings
 */
struct plugin_signal_settings {
	unsigned int  bits_per_sample; /**< size in bit of a converted voltage
	                                  (10 bits for the mini2440) */
	float         min_voltage;    /**< level in Volt for min value of signal */
	float         max_voltage;    /**< level in Volt for max value of signal */
	unsigned int  period;         /**< period in us */
};

/**
 * A plugin can compute (and return) a signal or a voltage. This list can of
 * course be extended in a new API version.
 */
enum plugin_type
{
	PLUGIN_RET_INT,
	PLUGIN_RET_SIGNAL,
};

/**
 * Structure exported from a library (*.so)
 */
struct plugin
{
	int           version;
	const char    *name;
	const char    *author;

	union {
		struct plugin_ops_v1        *v1;
		/* new versions can be added here */
	} plugin_ops; /**< operations of the module
	                 (depends on the version) */
};

/**
 * Type of the plugin private data
 */
typedef struct plugin_private t_plugin_private;

/**
 * Plugin operations of the v1.0 API
 */
struct plugin_ops_v1
{
	const char    *help;

	/// Init the plugin and eventually allocate data
	t_plugin_private* (*init)(void);
	/// reset the plugin (necessary before using it)
	void (*reset)(t_plugin_private *plugin_ctx,
	              struct plugin_signal_settings *settings);
	/// destroy allocated data
	void (*destroy)(t_plugin_private *plugin_ctx);

	enum plugin_type      type;   /**< type of the plugin */
	unsigned int          nb_args; /**< number of signal given in input
	                                 (process function) */
	const char            *label; /**< Label of output data */
	const char            *unit;  /**< Unit of output data */
	union {
		int (*ret_signal)(t_plugin_private *plugin_ctx,
		                  const struct plugin_signal *in_signals,
		                  struct plugin_signal *res_signal);
		int (*ret_int)(t_plugin_private *plugin_ctx,
		               const struct plugin_signal *in_signals,
		               int *res);
	} process;                    /**< calculation function (the prototype
	                                 depends on the plugin type) */
};

#endif /* _PLUGIN_H_ */
