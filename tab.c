#include <Ecore.h>
#include <Ecore_Evas.h>

#include <stdio.h>
#include <stdarg.h>

#include "scope.h"

#define _tab_type "Tab"
#define EVT_TAB_CLICKED "tab,clicked"

typedef struct _Tab_Data Tab_Data;

/*
 * This structure augments clipped smart object's instance data
 */
struct _Tab_Data
{
   Evas_Object_Smart_Clipped_Data base;
   Evas_Object *vline[6], *vrect;
   Evas_Object *rect;
   Evas_Object *title;
   Evas_Object *info;
};

#define TAB_DATA_GET(o, ptr) \
  Tab_Data * ptr = evas_object_smart_data_get(o)

#define TAB_DATA_GET_OR_RETURN(o, ptr)        \
  TAB_DATA_GET(o, ptr);                       \
  if (!ptr)                                                  \
    {                                                        \
       fprintf(stderr, "No widget data for object %p (%s)!", \
               o, evas_object_type_get(o));                  \
       fflush(stderr);                                       \
       abort();                                              \
       return;                                               \
    }

#define TAB_DATA_GET_OR_RETURN_VAL(o, ptr, val) \
  TAB_DATA_GET(o, ptr);                         \
  if (!ptr)                                                    \
    {                                                          \
       fprintf(stderr, "No widget data for object %p (%s)!",   \
               o, evas_object_type_get(o));                    \
       fflush(stderr);                                         \
       abort();                                                \
       return val;                                             \
    }

EVAS_SMART_SUBCLASS_NEW(_tab_type, _tab,
                        Evas_Smart_Class, Evas_Smart_Class,
                        evas_object_smart_clipped_class_get, NULL);


static void
_on_mousedown(void *data, Evas *e, Evas_Object *o, void *einfo)
{
	evas_object_smart_callback_call(data, "tab,mousedown", einfo);
}

/* create and setup a new tab object's internals */
static void
_tab_smart_add(Evas_Object *o)
{
	EVAS_SMART_DATA_ALLOC(o, Tab_Data);
	Evas *e = evas_object_evas_get(o);
	Evas_Object *clipper;
	int i;

	evas_object_size_hint_min_set(o, 62, 14);
	evas_object_size_hint_padding_set(o, 0, 0, 0, 1);

	_tab_parent_sc->add(o);
	clipper = evas_object_smart_clipped_clipper_get(o);

	for (i = 0 ; i < 6 ; ++i) {
		priv->vline[i] = evas_object_line_add(e);
		evas_object_clip_set(priv->vline[i], clipper);
		evas_object_color_set(priv->vline[i], DARKGRAY);
		evas_object_show(priv->vline[i]);
		evas_object_event_callback_add(priv->vline[i], EVAS_CALLBACK_MOUSE_DOWN, _on_mousedown, o);
	}
	
	priv->vrect = evas_object_rectangle_add(e);
	evas_object_clip_set(priv->vrect, clipper);
	evas_object_color_set(priv->vrect, DARKGRAY);
	evas_object_show(priv->vrect);
	evas_object_event_callback_add(priv->vrect, EVAS_CALLBACK_MOUSE_DOWN, _on_mousedown, o);

	priv->rect = evas_object_rectangle_add(e);
	evas_object_clip_set(priv->rect, clipper);
	evas_object_color_set(priv->rect, GRAY);
	evas_object_show(priv->rect);
	evas_object_pass_events_set(priv->rect, EINA_TRUE);

	priv->title = evas_object_text_add(e);
	evas_object_clip_set(priv->title, clipper);
	evas_object_color_set(priv->title, BLACK);
	evas_object_text_font_source_set(priv->title, FONT_EET);
	evas_object_text_font_set(priv->title, FONT ":style=Bold", 11);
	evas_object_text_text_set(priv->title, "TITLE");
	evas_object_anti_alias_set(priv->title, EINA_FALSE);
	evas_object_show(priv->title);
	evas_object_pass_events_set(priv->title, EINA_TRUE);

	priv->info = evas_object_text_add(e);
	evas_object_clip_set(priv->info, clipper);
	evas_object_color_set(priv->info, BLACK);
	evas_object_text_font_source_set(priv->info, FONT_EET);
	evas_object_text_font_set(priv->info, FONT, 10);
	evas_object_text_text_set(priv->info, "INFO");
	evas_object_anti_alias_set(priv->info, EINA_FALSE);
	evas_object_pass_events_set(priv->info, EINA_TRUE);
}

static void
_tab_smart_del(Evas_Object *o)
{
	TAB_DATA_GET(o, priv);
	int i;

	for (i = 0 ; i < 6 ; ++i)
		evas_object_del(priv->vline[i]);
	evas_object_del(priv->vrect);
	evas_object_del(priv->rect);
	evas_object_del(priv->title);
	evas_object_del(priv->info);

	_tab_parent_sc->del(o);
}

static void
_tab_smart_move(Evas_Object *o, Evas_Coord x, Evas_Coord y)
{
	_tab_parent_sc->move(o, x, y);
	evas_object_smart_changed(o);
}

static void
_tab_smart_resize(Evas_Object *o,
                  Evas_Coord w,
                  Evas_Coord h)
{
	Evas_Coord ow, oh;

	evas_object_geometry_get(o, NULL, NULL, &ow, &oh);
	if ((ow == w) && (oh == h)) return;

	/* this will trigger recalculation */
	evas_object_smart_changed(o);
}

/* act on child objects' properties, before rendering */
static void
_tab_smart_calculate(Evas_Object *o)
{
	Evas_Coord x, y, w, h;

	TAB_DATA_GET_OR_RETURN(o, priv);

	evas_object_geometry_get(o, &x, &y, &w, &h);

	evas_object_line_xy_set(priv->vline[0], x+0, y+3, x+0, y + h - 4);
	evas_object_line_xy_set(priv->vline[1], x+1, y+1, x+1, y + h - 2);
	evas_object_line_xy_set(priv->vline[2], x+2, y+1, x+2, y + h - 2);
	evas_object_line_xy_set(priv->vline[3], x+3, y+0, x+3, y + h - 1);
	evas_object_line_xy_set(priv->vline[4], x+4, y+0, x+4, y + h - 1);
	evas_object_line_xy_set(priv->vline[5], x+5, y+0, x+5, y + h - 1);

	evas_object_move(priv->vrect, x+5, y);
	evas_object_resize(priv->vrect, w-5, h);

	evas_object_move(priv->rect, x+6, y+1);
	evas_object_resize(priv->rect, w-7, h-2);

	evas_object_move(priv->title, x+5, y+1);
	evas_object_move(priv->info, x+6, y + h/2);
}

static void
_tab_smart_color_set(Evas_Object *o, int r, int g, int b, int a)
{
	TAB_DATA_GET(o, priv);
	int i;

	for (i = 0 ; i < 6 ; ++i)
		evas_object_color_set(priv->vline[i], r, g, b, a);
	evas_object_color_set(priv->vrect, r, g, b, a);
}

/* setting our smart interface */
static void
_tab_smart_set_user(Evas_Smart_Class *sc)
{
	/* specializing these */
	sc->add = _tab_smart_add;
	sc->del = _tab_smart_del;
	sc->move = _tab_smart_move;
	sc->color_set = _tab_smart_color_set;

	/* clipped smart object has no hook on resizes or calculations */
	sc->resize = _tab_smart_resize;
	sc->calculate = _tab_smart_calculate;
}

/* BEGINS example smart object's own interface */

/* add a new example smart object to a canvas */
Evas_Object *
tab_add(Evas *evas)
{
	return evas_object_smart_add(evas, _tab_smart_class_new());
}

void
tab_title_set(Evas_Object *o, const char *fmt, ...)
{
	va_list ap;
	char s[20];
	TAB_DATA_GET(o, priv);
	va_start(ap, fmt);
	vsnprintf(s, 20, fmt, ap);
	va_end(ap);
	s[19] = 0;
	evas_object_text_text_set(priv->title, s);
}

void
tab_title_color_set(Evas_Object *o, int r, int g, int b, int a)
{
	TAB_DATA_GET(o, priv);
	evas_object_color_set(priv->title, r, g, b, a);
}

void
tab_info_set(Evas_Object *o, const char *fmt, ...)
{
	TAB_DATA_GET(o, priv);
	if (fmt) {
		va_list ap;
		char s[20];
		va_start(ap, fmt);
		vsnprintf(s, 20, fmt, ap);
		va_end(ap);
		s[19] = 0;
		evas_object_text_text_set(priv->info, s);
		evas_object_size_hint_min_set(o, 62, 23);
		evas_object_show(priv->info);
	} else {
		evas_object_size_hint_min_set(o, 62, 14);
		evas_object_hide(priv->info);
	}
}
