#include <Ecore.h>
#include <Ecore_Evas.h>

#include <stdio.h>

#include "scope.h"
#include "tab.h"
#include "acq.h"
#include "module.h"
#include "record.h"
#include "display.h"

static void menu_push(Evas_Object *o);
static void menu_pop(void);
static int menu_animating;

#define ANIMATION_RUNTIME 0.2

static void
_delete_refresh(void *data, Evas *e, Evas_Object *o, void *einfo)
{
	ecore_event_handler_del(data);
}

static void
_on_back(void *data, Evas_Object *o, void *einfo)
{
	if (menu_animating) return;
	menu_pop();
}

static void
_show_menu_tab_title_source(void *data, Evas *e, Evas_Object *o, void *einfo)
{
	Channel *ch = data;
	tab_title_set(o, "%s", ch->name);
}

static void
_show_menu_tab_info_calib(void *data, Evas *e, Evas_Object *o, void *einfo)
{
	Channel *ch = data;

	if (ch-chan == ACQ_CHANNELS && plugin && plugin->plugin_ops.v1->type == PLUGIN_RET_INT) {
		extern int result_int;
		tab_info_set(o, "%d %s", result_int, plugin->plugin_ops.v1->unit);
	} else {
		if (ch->vdiv < 0.1) {
			tab_info_set(o, "%d mV/d", (int) (ch->vdiv * 1000));
		} else {
			tab_info_set(o, "%1.1f V/d", ch->vdiv);
		}
	}
}

static void
_show_menu_tab_info_calib_list(void *data, Evas *e, Evas_Object *o, void *einfo)
{
	Channel *ch = data;
		
	if (ch->visible)
		_show_menu_tab_info_calib(data, e, o, einfo);
	else
		tab_info_set(o, "Off");
}

static Eina_Bool
_show_menu_tab_info_calib_list_refresh(void *data, int type, void *ev)
{
	Channel *ch = evas_object_data_get(data, "channel");
	_show_menu_tab_info_calib_list(ch, NULL, data, NULL);
	return EINA_TRUE;
}

static void
_show_menu_tab_info_invert(void *data, Evas *e, Evas_Object *o, void *einfo)
{
	Channel *ch = data;
	tab_info_set(o, ch->invert ? "YES" : "NO");
}

static void
_show_menu_tab_info_visible(void *data, Evas *e, Evas_Object *o, void *einfo)
{
	Channel *ch = data;
	tab_info_set(o, ch->visible ? "YES" : "NO");
}

static Eina_Bool
_show_menu_tab_info_visible_refresh(void *data, int type, void *ev)
{
	Channel *ch = evas_object_data_get(data, "channel");
	_show_menu_tab_info_visible(ch, NULL, data, NULL);
	return EINA_TRUE;
}

static void
_show_menu_tab_info_acdc(void *data, Evas *e, Evas_Object *o, void *einfo)
{
	Channel *ch = data;
	tab_info_set(o, ch->ac_ndc ? "AC" : "DC");
}

static Eina_Bool
_show_menu_tab_info_acdc_refresh(void *data, int type, void *ev)
{
	Channel *ch = evas_object_data_get(data, "channel");
	_show_menu_tab_info_acdc(ch, NULL, data, NULL);
	return EINA_TRUE;
}

static void
_custom_split_layout(Evas_Object *o, Evas_Object_Box_Data *p, void *data)
{
	int x, y, w, h, h2;
	Evas_Object_Box_Option *opt;

	evas_object_geometry_get(o, &x, &y, &w, &h);

	Eina_List *l = p->children;
	opt = eina_list_data_get(l);
	evas_object_move(opt->obj, x, y);

	l = eina_list_next(l);
	opt = eina_list_data_get(l);
	evas_object_geometry_get(opt->obj, NULL, NULL, NULL, &h2);
	evas_object_move(opt->obj, x, y + h - h2);
}


// Acquisition ////////////////////////

int freq_t[] = {
	10000, 20000, 40000,
	60000, 80000, 90000,
	100000, 120000, 125000,
	8000, 11025, 22050, 44100, 48000
};

static void
_on_freq(void *data, Evas_Object *o, void *einfo)
{
	if (recording) return;
	int freq = * (int *) evas_object_data_get(o, "freq");
	acq_set_freq(freq);
	ecore_event_add(REFRESH_MENU, NULL, NULL, NULL);
	display_update();
}

static void
_show_menu_freq_title(void *data, Evas *e, Evas_Object *o, void *einfo)
{
	int _freq = * (int *) evas_object_data_get(o, "freq");
	tab_title_set(o, "%5dHz", _freq);
	if (acq_freq == _freq)
		evas_object_color_set(o, WHITE);
	else
		evas_object_color_set(o, DARKGRAY);
}

static Eina_Bool
_show_menu_freq_title_refresh(void *data, int type, void *ev)
{
	_show_menu_freq_title(NULL, NULL, data, NULL);
	return EINA_TRUE;
}

static Evas_Object *
menu_acq_config(Evas *e)
{
	Evas_Object *menu, *m;
	Evas_Object *tab;
	Ecore_Event_Handler *event;
	int i;
	
	menu = evas_object_box_add(e);
	evas_object_box_layout_set(menu, _custom_split_layout, NULL, NULL);
	evas_object_resize(menu, 55, 240);
	evas_object_box_align_set(menu, 0, 0);

	m = evas_object_box_add(e);
	evas_object_box_layout_set(m, evas_object_box_layout_vertical, NULL, NULL);
	evas_object_box_align_set(m, 0, 0);
	evas_object_box_append(menu, m);
	evas_object_show(m);

	for (i = 0 ; i < sizeof(freq_t)/sizeof(*freq_t) ; ++i) {
		tab = tab_add(e);
		evas_object_data_set(tab, "freq", &freq_t[i]);
		evas_object_smart_callback_add(tab, "tab,mousedown", _on_freq, NULL);
		evas_object_event_callback_add(tab, EVAS_CALLBACK_SHOW, _show_menu_freq_title, NULL);
		event = ecore_event_handler_add(REFRESH_MENU, _show_menu_freq_title_refresh, tab);
		evas_object_event_callback_add(tab, EVAS_CALLBACK_DEL, _delete_refresh, event);
		evas_object_show(tab);
		evas_object_box_append(m, tab);
	}


	/* Bottom: Back */
	m = evas_object_box_add(e);
	evas_object_box_layout_set(m, evas_object_box_layout_vertical, NULL, NULL);
	evas_object_box_align_set(m, 0, 1);
	evas_object_box_append(menu, m);
	evas_object_show(m);

	tab = tab_add(e);
	tab_title_set(tab, "Back");
	tab_info_set(tab, "");
	evas_object_color_set(tab, 85, 85, 85, 255);
	evas_object_show(tab);
	evas_object_smart_callback_add(tab, "tab,mousedown", _on_back, NULL);
	evas_object_box_append(m, tab);

	return menu;
}


// Time resolution ////////////////////

static void
_on_tdiv(void *data, Evas_Object *o, void *einfo)
{
	tdiv = * (float *) evas_object_data_get(o, "tdiv");
	ecore_event_add(REFRESH_MENU, NULL, NULL, NULL);
	display_update();
}

static void
_show_menu_tdiv_title(void *data, Evas *e, Evas_Object *o, void *einfo)
{
	float _tdiv = * (float *) evas_object_data_get(o, "tdiv");
	if (_tdiv < 0.0001) {
		tab_title_set(o, "%2d us/d", (int) (_tdiv * 1000000));
	} else if (_tdiv < 0.001) {
		tab_title_set(o, ".%d ms/d", (int) (_tdiv * 10000));
	} else {
		tab_title_set(o, "%2d ms/d", (int) (_tdiv * 1000));
	}
	if (tdiv == _tdiv)
		evas_object_color_set(o, WHITE);
	else
		evas_object_color_set(o, DARKGRAY);
}

static Eina_Bool
_show_menu_tdiv_title_refresh(void *data, int type, void *ev)
{
	_show_menu_tdiv_title(NULL, NULL, data, NULL);
	return EINA_TRUE;
}

static Evas_Object *
menu_time_config(Evas *e)
{
	Evas_Object *menu, *m;
	Evas_Object *tab;
	Ecore_Event_Handler *event;
	int i;
	
	menu = evas_object_box_add(e);
	evas_object_box_layout_set(menu, _custom_split_layout, NULL, NULL);
	evas_object_resize(menu, 55, 240);
	evas_object_box_align_set(menu, 0, 0);

	m = evas_object_box_add(e);
	evas_object_box_layout_set(m, evas_object_box_layout_vertical, NULL, NULL);
	evas_object_box_align_set(m, 0, 0);
	evas_object_box_append(menu, m);
	evas_object_show(m);

	for (i = 0 ; i < sizeof(calib_t)/sizeof(*calib_t) ; ++i) {
		tab = tab_add(e);
		evas_object_data_set(tab, "tdiv", &calib_t[i]);
		evas_object_smart_callback_add(tab, "tab,mousedown", _on_tdiv, NULL);
		evas_object_event_callback_add(tab, EVAS_CALLBACK_SHOW, _show_menu_tdiv_title, NULL);
		event = ecore_event_handler_add(REFRESH_MENU, _show_menu_tdiv_title_refresh, tab);
		evas_object_event_callback_add(tab, EVAS_CALLBACK_DEL, _delete_refresh, event);
		evas_object_show(tab);
		evas_object_box_append(m, tab);
	}


	/* Bottom: Back */
	m = evas_object_box_add(e);
	evas_object_box_layout_set(m, evas_object_box_layout_vertical, NULL, NULL);
	evas_object_box_align_set(m, 0, 1);
	evas_object_box_append(menu, m);
	evas_object_show(m);

	tab = tab_add(e);
	tab_title_set(tab, "Back");
	tab_info_set(tab, "");
	evas_object_color_set(tab, 85, 85, 85, 255);
	evas_object_show(tab);
	evas_object_smart_callback_add(tab, "tab,mousedown", _on_back, NULL);
	evas_object_box_append(m, tab);

	return menu;
}


// Voltage calibration ////////////////

static void
_on_vdiv(void *data, Evas_Object *o, void *einfo)
{
	Channel *ch = data;
	float vdiv = * (float *) evas_object_data_get(o, "vdiv");
	ch->vdiv = vdiv;
	ecore_event_add(REFRESH_MENU, NULL, NULL, NULL);
	display_update();
}

static void
_show_menu_calib_title(void *data, Evas *e, Evas_Object *o, void *einfo)
{
	Channel *ch = data;
	float vdiv = * (float *) evas_object_data_get(o, "vdiv");
	if (vdiv < 0.1) {
		tab_title_set(o, "%d mV/d", (int) (vdiv * 1000));
	} else {
		tab_title_set(o, "%1.1f V/d", vdiv);
	}
	
	if (ch->vdiv == vdiv) {
		color *c = &colors[ch - chan];
		evas_object_color_set(o, c->r, c->g, c->b, c->a);
	} else {
		evas_object_color_set(o, DARKGRAY);
	}
}

static Eina_Bool
_show_menu_calib_title_refresh(void *data, int type, void *ev)
{
	Channel *ch = evas_object_data_get(data, "channel");
	_show_menu_calib_title(ch, NULL, data, NULL);
	return EINA_TRUE;
}

// Plugin configuration ///////////////

static void
_show_menu_plugin_title(void *data, Evas *e, Evas_Object *o, void *einfo)
{
	if (data != plugin)
		evas_object_color_set(o, DARKGRAY);
	else
		evas_object_color_set(o, WHITE);
}

static Eina_Bool
_show_menu_plugin_title_refresh(void *data, int type, void *ev)
{
	void *p = evas_object_data_get(data, "plugin");
	_show_menu_plugin_title(p, NULL, data, NULL);
	return EINA_TRUE;
}

static void
_show_menu_tab_info_arg(void *data, Evas *e, Evas_Object *o, void *einfo)
{
	int *arg = data;
	color *c = &colors[*arg];
	tab_info_set(o, "ACQ%d", *arg + 1);
	evas_object_color_set(o, c->r, c->g, c->b, c->a);
	if (plugin && (arg - plugin_args) < plugin_nb_args_get())
		evas_object_show(o);
	else
		evas_object_hide(o);
}

static Eina_Bool
_show_menu_tab_info_arg_refresh(void *data, int type, void *ev)
{
	void *arg = evas_object_data_get(data, "arg");
	_show_menu_tab_info_arg(arg, NULL, data, NULL);
	return EINA_TRUE;
}

static void
_on_plugin_name(void *data, Evas_Object *o, void *einfo)
{
	if (mod_recording) return;
	module_select(plugin == data ? NULL : data);
	chan[ACQ_CHANNELS].visible = plugin != NULL;
	ecore_event_add(REFRESH_MENU, NULL, NULL, NULL);
}

static void
_on_plugin_arg(void *data, Evas_Object *o, void *einfo)
{
	int *arg = data;
	if (mod_recording) return;
	*arg = (*arg + 1) % ACQ_CHANNELS;
	ecore_event_add(REFRESH_MENU, NULL, NULL, NULL);
}

static Evas_Object *
menu_plugin(Evas *e)
{
	Evas_Object *menu, *m;
	Evas_Object *tab;
	Ecore_Event_Handler *event;
	Eina_List *l;
	struct plugin *p;
	int i;

	menu = evas_object_box_add(e);
	evas_object_box_layout_set(menu, _custom_split_layout, NULL, NULL);
	evas_object_resize(menu, 55, 240);
	evas_object_box_align_set(menu, 0, 0);

	m = evas_object_box_add(e);
	evas_object_box_layout_set(m, evas_object_box_layout_vertical, NULL, NULL);
	evas_object_box_align_set(m, 0, 0);
	evas_object_box_append(menu, m);
	evas_object_show(m);

	EINA_LIST_FOREACH(plugin_list, l, p) {
		tab = tab_add(e);
		tab_title_set(tab, p->name);
		evas_object_data_set(tab, "plugin", p);
		evas_object_smart_callback_add(tab, "tab,mousedown", _on_plugin_name, p);
		evas_object_event_callback_add(tab, EVAS_CALLBACK_SHOW, _show_menu_plugin_title, p);
		event = ecore_event_handler_add(REFRESH_MENU, _show_menu_plugin_title_refresh, tab);
		evas_object_event_callback_add(tab, EVAS_CALLBACK_DEL, _delete_refresh, event);
		evas_object_show(tab);
		evas_object_box_append(m, tab);
	}

	for (i = 0 ; i < ACQ_CHANNELS ; ++i) {
		tab = tab_add(e);
		tab_title_set(tab, "Arg #%d", i + 1);
		evas_object_data_set(tab, "arg", &plugin_args[i]);
		evas_object_event_callback_add(tab, EVAS_CALLBACK_SHOW, _show_menu_tab_info_arg, &plugin_args[i]);
		evas_object_smart_callback_add(tab, "tab,mousedown", _on_plugin_arg, &plugin_args[i]);
		event = ecore_event_handler_add(REFRESH_MENU, _show_menu_tab_info_arg_refresh, tab);
		evas_object_event_callback_add(tab, EVAS_CALLBACK_DEL, _delete_refresh, event);
		if (plugin && i < plugin_nb_args_get())
			evas_object_show(tab);
		evas_object_box_append(m, tab);
	}

	/* Bottom: Back */
	m = evas_object_box_add(e);
	evas_object_box_layout_set(m, evas_object_box_layout_vertical, NULL, NULL);
	evas_object_box_align_set(m, 0, 1);
	evas_object_box_append(menu, m);
	evas_object_show(m);

	tab = tab_add(e);
	tab_title_set(tab, "Back");
	tab_info_set(tab, "");
	evas_object_color_set(tab, 85, 85, 85, 255);
	evas_object_show(tab);
	evas_object_smart_callback_add(tab, "tab,mousedown", _on_back, NULL);
	evas_object_box_append(m, tab);

	return menu;
}

// Channel configuration //////////////

static void
_on_channel_invert(void *data, Evas_Object *o, void *einfo)
{
	if (menu_animating) return;
	Channel *ch = data;
	ch->invert = !ch->invert;
	tab_info_set(o, ch->invert ? "YES" : "NO");
	display_update();
}

static void
_on_channel_visible(void *data, Evas_Object *o, void *einfo)
{
	if (menu_animating) return;
	Channel *ch = data;
	ch->visible = !ch->visible;
	if (ch->visible && ch == &chan[ACQ_CHANNELS] && plugin == NULL)
		ch->visible = 0;
	tab_info_set(o, ch->visible ? "YES" : "NO");
	ecore_event_add(REFRESH_MENU, NULL, NULL, NULL);
	display_update();
}

static void
_on_channel_acdc(void *data, Evas_Object *o, void *einfo)
{
	if (menu_animating) return;
	Channel *ch = data;
	ch->ac_ndc = !ch->ac_ndc;
	tab_info_set(o, ch->ac_ndc ? "AC" : "DC");
	ecore_event_add(REFRESH_MENU, NULL, NULL, NULL);
	display_update();
}

static void
_on_channel_plugin(void *data, Evas_Object *o, void *einfo)
{
	if (menu_animating) return;
	Evas_Object *m = menu_plugin(evas_object_evas_get(o));
	menu_push(m);
}

static void
_show_menu_tab_info_plugin(void *data, Evas *e, Evas_Object *o, void *einfo)
{
	tab_info_set(o, plugin ? plugin->name : "-");
}

static Eina_Bool
_show_menu_tab_info_plugin_refresh(void *data, int type, void *ev)
{
	_show_menu_tab_info_plugin(NULL, NULL, data, NULL);
	return EINA_TRUE;
}


static Evas_Object *
menu_channel_config(Evas *e, Channel *ch)
{
	Evas_Object *menu, *m;
	Evas_Object *tab;
	color *c = &colors[ch - chan];
	Ecore_Event_Handler *event;
	int i;
	
	menu = evas_object_box_add(e);
	evas_object_box_layout_set(menu, _custom_split_layout, NULL, NULL);
	evas_object_resize(menu, 55, 240);
	evas_object_box_align_set(menu, 0, 0);

	m = evas_object_box_add(e);
	evas_object_box_layout_set(m, evas_object_box_layout_vertical, NULL, NULL);
	evas_object_box_align_set(m, 0, 0);
	evas_object_box_append(menu, m);
	evas_object_show(m);

	if ((ch - chan) == ACQ_CHANNELS) {
		tab = tab_add(e);
		tab_title_set(tab, "Plugin");
		evas_object_event_callback_add(tab, EVAS_CALLBACK_SHOW, _show_menu_tab_info_plugin, NULL);
		evas_object_smart_callback_add(tab, "tab,mousedown", _on_channel_plugin, ch);
		event = ecore_event_handler_add(REFRESH_MENU, _show_menu_tab_info_plugin_refresh, tab);
		evas_object_event_callback_add(tab, EVAS_CALLBACK_DEL, _delete_refresh, event);
		evas_object_color_set(tab, c->r, c->g, c->b, c->a);
		evas_object_show(tab);
		evas_object_box_append(m, tab);
	} else {
		tab = tab_add(e);
		evas_object_data_set(tab, "channel", ch);
		tab_title_set(tab, "Visible");
		evas_object_event_callback_add(tab, EVAS_CALLBACK_SHOW, _show_menu_tab_info_visible, ch);
		evas_object_smart_callback_add(tab, "tab,mousedown", _on_channel_visible, ch);
		event = ecore_event_handler_add(REFRESH_MENU, _show_menu_tab_info_visible_refresh, tab);
		evas_object_event_callback_add(tab, EVAS_CALLBACK_DEL, _delete_refresh, event);
		evas_object_color_set(tab, c->r, c->g, c->b, c->a);
		evas_object_show(tab);
		evas_object_box_append(m, tab);
	}

	tab = tab_add(e);
	tab_title_set(tab, "Invert");
	evas_object_event_callback_add(tab, EVAS_CALLBACK_SHOW, _show_menu_tab_info_invert, ch);
	evas_object_smart_callback_add(tab, "tab,mousedown", _on_channel_invert, ch);
	evas_object_color_set(tab, c->r, c->g, c->b, c->a);
	evas_object_show(tab);
	evas_object_box_append(m, tab);

	tab = tab_add(e);
	evas_object_data_set(tab, "channel", ch);
	tab_title_set(tab, "Coupling");
	evas_object_event_callback_add(tab, EVAS_CALLBACK_SHOW, _show_menu_tab_info_acdc, ch);
	evas_object_smart_callback_add(tab, "tab,mousedown", _on_channel_acdc, ch);
	event = ecore_event_handler_add(REFRESH_MENU, _show_menu_tab_info_acdc_refresh, tab);
	evas_object_event_callback_add(tab, EVAS_CALLBACK_DEL, _delete_refresh, event);
	evas_object_color_set(tab, c->r, c->g, c->b, c->a);
	evas_object_show(tab);
	evas_object_box_append(m, tab);

	for (i = 0 ; i < sizeof(calib_v)/sizeof(*calib_v) ; ++i) {
		tab = tab_add(e);
		evas_object_data_set(tab, "channel", ch);
		evas_object_data_set(tab, "vdiv", &calib_v[i]);
		evas_object_smart_callback_add(tab, "tab,mousedown", _on_vdiv, ch);
		evas_object_event_callback_add(tab, EVAS_CALLBACK_SHOW, _show_menu_calib_title, ch);
		event = ecore_event_handler_add(REFRESH_MENU, _show_menu_calib_title_refresh, tab);
		evas_object_event_callback_add(tab, EVAS_CALLBACK_DEL, _delete_refresh, event);
		evas_object_show(tab);
		evas_object_box_append(m, tab);
	}

	/* Bottom: Back */
	m = evas_object_box_add(e);
	evas_object_box_layout_set(m, evas_object_box_layout_vertical, NULL, NULL);
	evas_object_box_align_set(m, 0, 1);
	evas_object_box_append(menu, m);
	evas_object_show(m);

	tab = tab_add(e);
	tab_title_set(tab, "Back");
	tab_info_set(tab, "");
	evas_object_color_set(tab, 85, 85, 85, 255);
	evas_object_show(tab);
	evas_object_smart_callback_add(tab, "tab,mousedown", _on_back, NULL);
	evas_object_box_append(m, tab);

	return menu;
}

// Trigger menu ///////////////////////

static void
_on_trigger_source(void *data, Evas_Object *o, void *einfo)
{
	Channel *ch = data;
	if (menu_animating) return;
	trigger_chan = ch - chan;
	trigger_ypos = display_trigger_ypos_get() + 5;
	ecore_event_add(REFRESH_MENU, NULL, NULL, NULL);
	display_update();
}

static void
_show_menu_trigger_source(void *data, Evas *e, Evas_Object *o, void *einfo)
{
	Channel *ch = data;
	color *c = &colors[ch - chan];
	tab_title_set(o, "%s", ch->name);
	tab_title_color_set(o, c->r, c->g, c->b, c->a);
	if ((ch - chan) == trigger_chan) {
		c = &colors[trigger_chan];
		evas_object_color_set(o, c->r, c->g, c->b, c->a);
	} else {
		evas_object_color_set(o, DARKGRAY);
	}
}

static Eina_Bool
_show_menu_trigger_source_refresh(void *data, int type, void *ev)
{
	Channel *ch = evas_object_data_get(data, "channel");
	_show_menu_trigger_source(ch, NULL, data, NULL);
	return EINA_TRUE;
}

static void
_on_trigger_sense(void *data, Evas_Object *o, void *einfo)
{
	if (menu_animating) return;
	trigger_sense = !trigger_sense;
	tab_info_set(o, trigger_sense ? "Incr." : "Decr");
	display_update();
}

static void
_show_menu_tab_info_trigger_source(void *data, Evas *e, Evas_Object *o, void *einfo)
{
	color *c = &colors[trigger_chan];
	tab_info_set(o, "%s", chan[trigger_chan].name);
	evas_object_color_set(o, c->r, c->g, c->b, c->a);
}

static Eina_Bool
_show_menu_tab_info_trigger_source_refresh(void *data, int type, void *ev)
{
	_show_menu_tab_info_trigger_source(NULL, NULL, data, NULL);
	return EINA_TRUE;
}

static void
_show_menu_tab_info_trigger_sense(void *data, Evas *e, Evas_Object *o, void *einfo)
{
	tab_info_set(o, trigger_sense ? "Incr." : "Decr");
}

static Evas_Object *
menu_trigger(Evas *e)
{
	Evas_Object *menu, *m;
	Evas_Object *tab;
	Ecore_Event_Handler *event;
	int i;
	
	menu = evas_object_box_add(e);
	evas_object_box_layout_set(menu, _custom_split_layout, NULL, NULL);
	evas_object_resize(menu, 55, 240);
	evas_object_box_align_set(menu, 0, 0);

	/* Top: Trigger options */
	m = evas_object_box_add(e);
	evas_object_box_layout_set(m, evas_object_box_layout_vertical, NULL, NULL);
	evas_object_box_align_set(m, 0, 0);
	evas_object_box_append(menu, m);
	evas_object_show(m);

	tab = tab_add(e);
	tab_title_set(tab, "Sense");
	evas_object_event_callback_add(tab, EVAS_CALLBACK_SHOW, _show_menu_tab_info_trigger_sense, NULL);
	evas_object_smart_callback_add(tab, "tab,mousedown", _on_trigger_sense, NULL);
	evas_object_show(tab);
	evas_object_box_append(m, tab);

	for (i = 0 ; i < ACQ_CHANNELS ; ++i) {
		Channel *ch = &chan[i];
		tab = tab_add(e);
		evas_object_data_set(tab, "channel", ch);
		evas_object_smart_callback_add(tab, "tab,mousedown", _on_trigger_source, ch);
		evas_object_event_callback_add(tab, EVAS_CALLBACK_SHOW, _show_menu_trigger_source, ch);
		event = ecore_event_handler_add(REFRESH_MENU, _show_menu_trigger_source_refresh, tab);
		evas_object_event_callback_add(tab, EVAS_CALLBACK_DEL, _delete_refresh, event);
		evas_object_show(tab);
		evas_object_box_append(m, tab);
	}

	/* Bottom: Back */
	m = evas_object_box_add(e);
	evas_object_box_layout_set(m, evas_object_box_layout_vertical, NULL, NULL);
	evas_object_box_append(menu, m);
	evas_object_show(m);

	tab = tab_add(e);
	tab_title_set(tab, "Back");
	tab_info_set(tab, "");
	evas_object_color_set(tab, 85, 85, 85, 255);
	evas_object_show(tab);
	evas_object_smart_callback_add(tab, "tab,mousedown", _on_back, NULL);
	evas_object_box_append(m, tab);

	return menu;
}

// Channels list //////////////////////

static void
_on_channel(void *data, Evas_Object *o, void *einfo)
{
	if (menu_animating) return;
	Channel *ch = data;
	Evas_Object *m = menu_channel_config(evas_object_evas_get(o), ch);
	menu_push(m);
}

static void
_on_acq(void *data, Evas_Object *o, void *einfo)
{
	if (menu_animating) return;
	Evas_Object *m = menu_acq_config(evas_object_evas_get(o));
	menu_push(m);
}

static void
_on_time(void *data, Evas_Object *o, void *einfo)
{
	if (menu_animating) return;
	Evas_Object *m = menu_time_config(evas_object_evas_get(o));
	menu_push(m);
}

static void
_on_trigger(void *data, Evas_Object *o, void *einfo)
{
	if (menu_animating) return;
	Evas_Object *m = menu_trigger(evas_object_evas_get(o));
	menu_push(m);
}

static void
_show_menu_tab_info_freq(void *data, Evas *e, Evas_Object *o, void *einfo)
{
	tab_info_set(o, "%5d Hz", acq_freq);
}

static Eina_Bool
_show_menu_tab_info_freq_refresh(void *data, int type, void *ev)
{
	_show_menu_tab_info_freq(NULL, NULL, data, NULL);
	return EINA_TRUE;
}

static void
_show_menu_tab_info_time(void *data, Evas *e, Evas_Object *o, void *einfo)
{
	if (tdiv < 0.0001) {
		tab_info_set(o, "%2d us/d", (int) (tdiv * 1000000));
	} else if (tdiv < 0.001) {
		tab_info_set(o, ".%d ms/d", (int) (tdiv * 10000));
	} else {
		tab_info_set(o, "%2d ms/d", (int) (tdiv * 1000));
	}
}

static Eina_Bool
_show_menu_tab_info_time_refresh(void *data, int type, void *ev)
{
	_show_menu_tab_info_time(NULL, NULL, data, NULL);
	return EINA_TRUE;
}

static void
_show_menu_tab_info_trigger_tab(void *data, Evas *e, Evas_Object *o, void *einfo)
{
	int ypos = trigger_ypos - display_trigger_ypos_get();
	float val = ypos * chan[trigger_chan].vdiv / 25.0;
	if (val <= 0.1 && val >= -0.1) {
		tab_info_set(o, "%d mV", (int) (val * 1000));
	} else {
		tab_info_set(o, "%1.1f V", val);
	}
	color *c = &colors[trigger_chan];
	evas_object_color_set(o, c->r, c->g, c->b, c->a);
}

static Eina_Bool
_show_menu_tab_info_trigger_tab_refresh(void *data, int type, void *ev)
{
	_show_menu_tab_info_trigger_tab(NULL, NULL, data, NULL);
	return EINA_TRUE;
}

static Evas_Object *
menu_channel_list(Evas *e)
{
	Evas_Object *menu, *m;
	Evas_Object *tab;
	Ecore_Event_Handler *event;
	int i;
	
	menu = evas_object_box_add(e);
	evas_object_box_layout_set(menu, _custom_split_layout, NULL, NULL);
	evas_object_resize(menu, 55, 240);

	/* Top: Channel list */
	m = evas_object_box_add(e);
	evas_object_box_layout_set(m, evas_object_box_layout_vertical, NULL, NULL);
	evas_object_box_append(menu, m);
	evas_object_show(m);

	for (i = 0 ; i < CHANNELS ; ++i) {
		Channel *ch = chan + i;
		tab = tab_add(e);
		evas_object_data_set(tab, "channel", ch);
		evas_object_event_callback_add(tab, EVAS_CALLBACK_SHOW, _show_menu_tab_title_source, ch);
		evas_object_event_callback_add(tab, EVAS_CALLBACK_SHOW, _show_menu_tab_info_calib_list, ch);
		event = ecore_event_handler_add(REFRESH_MENU, _show_menu_tab_info_calib_list_refresh, tab);
		event = ecore_event_handler_add(REFRESH_MODULE_VALUE, _show_menu_tab_info_calib_list_refresh, tab);
		evas_object_event_callback_add(tab, EVAS_CALLBACK_DEL, _delete_refresh, event);
		evas_object_color_set(tab, colors[i].r, colors[i].g, colors[i].b, colors[i].a);
		evas_object_show(tab);
		evas_object_smart_callback_add(tab, "tab,mousedown", _on_channel, ch);
		evas_object_box_append(m, tab);
	}

	/* Bottom: Time & Trigger */
	m = evas_object_box_add(e);
	evas_object_box_layout_set(m, evas_object_box_layout_vertical, NULL, NULL);
	evas_object_box_append(menu, m);
	evas_object_show(m);

	tab = tab_add(e);
	tab_title_set(tab, "Acq.");
	evas_object_smart_callback_add(tab, "tab,mousedown", _on_acq, NULL);
	evas_object_event_callback_add(tab, EVAS_CALLBACK_SHOW, _show_menu_tab_info_freq, NULL);
	event = ecore_event_handler_add(REFRESH_MENU, _show_menu_tab_info_freq_refresh, tab);
	evas_object_event_callback_add(tab, EVAS_CALLBACK_DEL, _delete_refresh, event);
	evas_object_show(tab);
	evas_object_box_append(m, tab);

	tab = tab_add(e);
	tab_title_set(tab, "Time");
	evas_object_smart_callback_add(tab, "tab,mousedown", _on_time, NULL);
	evas_object_event_callback_add(tab, EVAS_CALLBACK_SHOW, _show_menu_tab_info_time, NULL);
	event = ecore_event_handler_add(REFRESH_MENU, _show_menu_tab_info_time_refresh, tab);
	evas_object_event_callback_add(tab, EVAS_CALLBACK_DEL, _delete_refresh, event);
	evas_object_show(tab);
	evas_object_box_append(m, tab);

	tab = tab_add(e);
	tab_title_set(tab, "Trig");
	evas_object_event_callback_add(tab, EVAS_CALLBACK_SHOW, _show_menu_tab_info_trigger_tab, NULL);
	evas_object_smart_callback_add(tab, "tab,mousedown", _on_trigger, NULL);
	event = ecore_event_handler_add(REFRESH_MENU, _show_menu_tab_info_trigger_tab_refresh, tab);
	evas_object_event_callback_add(tab, EVAS_CALLBACK_DEL, _delete_refresh, event);
	evas_object_show(tab);
	evas_object_box_append(m, tab);

	return menu;
}

// Menu stack /////////////////////////

typedef struct {
	Eina_Clist entry;
	Evas_Object *o;
} menu_entry;

static Eina_Clist menu_head = EINA_CLIST_INIT( menu_head );

static Eina_Bool
_on_animator_timeline_in(void *data, double pos)
{
	double frame = ecore_animator_pos_map(pos, ECORE_POS_MAP_DECELERATE, 0, 0);
	int x = 258 + 62 * (1.0 - frame);
	evas_object_move(data, x, 0);
	evas_object_show(data);
	return EINA_TRUE;
}

static Eina_Bool
_on_animator_timeline_out(void *data, double pos)
{
	double frame = ecore_animator_pos_map(pos, ECORE_POS_MAP_ACCELERATE, 0, 0);
	int x = 258 + 62 * frame;
	evas_object_move(data, x, 0);
	evas_object_show(data);
	return EINA_TRUE;
}

static void
_on_animator_in(void *data)
{
	menu_animating = 0;
}

static void
_on_animator_push_out(void *data)
{
	Evas_Object *o = data;

	menu_entry *m = malloc(sizeof *m);
	m->o = o;
	eina_clist_add_head(&menu_head, &m->entry);

	Ecore_Animator *a = ecore_animator_timeline_add(ANIMATION_RUNTIME, _on_animator_timeline_in, o);
	ecore_animator_end_func_set(a, _on_animator_in, o);
}

static void
_on_animator_pop_out(void *data)
{
	Evas_Object *o = data;
	evas_object_del(o);

	Eina_Clist *top = eina_clist_head(&menu_head);
	Ecore_Animator *a = ecore_animator_timeline_add(ANIMATION_RUNTIME, _on_animator_timeline_in, EINA_CLIST_ENTRY(top, menu_entry, entry)->o);
	ecore_animator_end_func_set(a, _on_animator_in, o);
}

void
menu_init(Evas *e)
{
	menu_push(menu_channel_list(e));
}

static void
menu_push(Evas_Object *o)
{
	Eina_Clist *top = eina_clist_head(&menu_head);
	menu_entry *m = EINA_CLIST_ENTRY(top, menu_entry, entry);
	if (m) {
		Ecore_Animator *a = ecore_animator_timeline_add(ANIMATION_RUNTIME, _on_animator_timeline_out, m->o);
		ecore_animator_end_func_set(a, _on_animator_push_out, o);
	} else {
		_on_animator_push_out(o);
	}

	menu_animating = 1;
}

static void
menu_pop(void)
{
	Eina_Clist *top = eina_clist_head(&menu_head);
	eina_clist_remove(top);

	menu_entry *m = EINA_CLIST_ENTRY(top, menu_entry, entry);
	Ecore_Animator *a = ecore_animator_timeline_add(ANIMATION_RUNTIME, _on_animator_timeline_out, m->o);
	ecore_animator_end_func_set(a, _on_animator_pop_out, m->o);
	free(m);

	menu_animating = 1;
}

