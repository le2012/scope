#include "plugin.h"

extern void module_init(void);
extern struct plugin *plugin;
extern Eina_List *plugin_list;
extern int plugin_args[ACQ_CHANNELS];
extern int plugin_nb_args_get(void);
extern void module_get(void);
extern void module_put(void);
extern t_plugin_private *display_ctxt, *record_ctxt;
