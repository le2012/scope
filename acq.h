typedef struct {
	uint64_t target;
	pthread_cond_t cond;
	pthread_mutex_t mutex;
} acq_countdown;

extern void *acquisition_thread(void *param);
extern void acq_add_countdown(acq_countdown *counter);
extern void acq_set_freq(int freq);

#define ACQ_CHANNELS 4
#define ACQ_MAX_FREQUENCY 100000
#define ACQ_BUFFER_SIZE (1<<20)
#define ACQ_SAMPLES (ACQ_BUFFER_SIZE/ACQ_CHANNELS/2)

extern volatile uint16_t *acq_samples;
extern volatile uint64_t acq_wpos;
extern volatile int acq_freq;
