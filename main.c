#include <Ecore.h>
#include <Ecore_Evas.h>

#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <pthread.h>
#include <time.h>

#include "tab.h"
#include "menus.h"
#include "scope.h"
#include "display.h"
#include "acq.h"
#include "record.h"
#include "module.h"

#define WIDTH 320
#define HEIGHT 240

color colors[] = {
	{ L_RED },	// Channels
	{ L_YELLOW },
	{ L_MAGENTA },
	{ L_CYAN },
	{ L_GREEN },
	{ WHITE },
	{ L_BLUE },
	{ DARKGRAY },	// grid foreground
};

int REFRESH_MENU;
int REFRESH_MODULE_VALUE;

static Ecore_Evas *ee;
static pthread_t acq_thread, dis_thread;
static Ecore_Timer *cpu_usage;
static Evas_Object *status;

static Eina_Bool
_on_cpu_timer(void *data)
{
	struct timespec ts;
	static uint32_t cpu_ns, acq_ns, dis_ns, ui_ns, total_ns;
	int cpu, acq, dis, ui, fps, delta;
	uint32_t ns;
	static int first = 1;
	clockid_t cid;

	/* total time */
	clock_gettime(CLOCK_MONOTONIC, &ts);
	ns = ts.tv_sec * 1000000000UL + ts.tv_nsec;
	delta = (ns - total_ns) / 100;
	total_ns = ns;

	/* Process usage */
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &ts);
	ns = ts.tv_sec * 1000000000UL + ts.tv_nsec;
	cpu = (ns - cpu_ns) / delta;
	cpu_ns = ns;

	/* Acquisition thread usage */
	pthread_getcpuclockid(acq_thread, &cid);
	clock_gettime(cid, &ts);
	ns = ts.tv_sec * 1000000000UL + ts.tv_nsec;
	acq = (ns - acq_ns) / delta;
	acq_ns = ns;

	/* UI thread usage */
	pthread_getcpuclockid(pthread_self(), &cid);
	clock_gettime(cid, &ts);
	ns = ts.tv_sec * 1000000000UL + ts.tv_nsec;
	ui = (ns - ui_ns) / delta;
	ui_ns = ns;

	/* Display thread usage */
	pthread_getcpuclockid(dis_thread, &cid);
	clock_gettime(cid, &ts);
	ns = ts.tv_sec * 1000000000UL + ts.tv_nsec;
	dis = (ns - dis_ns) / delta;
	dis_ns = ns;

	if (first) {
		first = 0;
	} else {
		char line[60] = {0};
		int fps = display_fps_get_reset();
		snprintf(line, 59, "CPU %2d%%|UI %2d%%|FPS %3d", cpu, ui, fps);
		//snprintf(line, 59, "CPU %2d%%|UI %2d%%|ACQ %2d%%|DIS %2d%%|FPS %3d", cpu, ui, acq, dis, fps);
		evas_object_text_text_set(status, line);
	}

	return EINA_TRUE;
}

static void
_on_destroy(Ecore_Evas *ee)
{
	ecore_main_loop_quit();
}

static void
_on_keydown(void *data, Evas *e, Evas_Object *o, void *einfo)
{
	Evas_Event_Key_Down *ev = einfo;
	if (strcmp(ev->keyname, "q") == 0)
		ecore_main_loop_quit();
}

// Ball support
void *ball_pixels;
Evas_Object *ball_o;
volatile int ball_off;
pthread_mutex_t ball_mutex;
pthread_cond_t ball_cond;

static void
_on_ball_mousedown(void *data, Evas *e, Evas_Object *o, void *einfo)
{
	ball_off = !ball_off;
	if (pthread_mutex_trylock(&ball_mutex) == 0) {
		pthread_cond_signal(&ball_cond);
		pthread_mutex_unlock(&ball_mutex);
	}
}

static void
ball_init(Evas *e)
{
	Evas_Object *o;
	void *pixels;
	extern void *ball(void *param);
	struct sched_param sp;
	pthread_t th;

	o = evas_object_image_filled_add(e);
	evas_object_lower(o);
	evas_object_move(o, 0, 0);
	evas_object_resize(o, 80, 32);
	evas_object_image_size_set(o, 80, 32);
	evas_object_event_callback_add(o, EVAS_CALLBACK_MOUSE_DOWN, _on_ball_mousedown, NULL);
	//evas_map_smooth_set(o, EINA_FALSE);
	//evas_object_image_scale_hint_set(o, EVAS_IMAGE_SCALE_HINT_DYNAMIC);
	pixels = evas_object_image_data_convert(o, EVAS_COLORSPACE_ARGB8888);
	if (pixels == NULL) pixels = evas_object_image_data_get(o, EINA_TRUE);
	evas_object_show(o);
	ball_o = o;
	ball_pixels = pixels;

	pthread_create(&th, NULL, ball, NULL);
#ifdef USE_PRIO
	sp.sched_priority = BALL_THREAD_PRIO;
	pthread_setschedparam(th, SCHED_RR, &sp);
#endif
}

int
main(int argc, char **argv)
{
	struct sched_param sp;
	Evas *evas;

	if (!ecore_evas_init())
	{
		printf("ERROR: Cannot init Ecore!\n");
		return EXIT_FAILURE;
	}

	REFRESH_MENU = ecore_event_type_new();
	REFRESH_MODULE_VALUE = ecore_event_type_new();

#ifdef USE_PRIO
	/* Set default priority */
	sp.sched_priority = DEFAULT_THREAD_PRIO;
	pthread_setschedparam(pthread_self(), SCHED_RR, &sp);
#endif

#if 0
	Eina_List *eng = ecore_evas_engines_get();
	Eina_List *l;
	char *data;
	EINA_LIST_FOREACH(eng, l, data) printf("%s\n", data);
#endif

	/* this will give you a window with an Evas canvas under the first
	 * engine available */
	ee = ecore_evas_new(NULL, 0, 0, WIDTH, HEIGHT, NULL);
	if (!ee) {
		fprintf(stderr, "You got to have at least one Evas engine built"
				" and linked up to ecore-evas for this example to run"
				" properly.\n");
		ecore_evas_shutdown();
		return EXIT_FAILURE;
	}

	ecore_evas_callback_delete_request_set(ee, _on_destroy);
	ecore_evas_title_set(ee, "Oscilloscope");
	//ecore_evas_callback_resize_set(ee, _canvas_resize_cb);
	ecore_evas_show(ee);

	evas = ecore_evas_get(ee);

	Evas_Object *bg = evas_object_rectangle_add(evas);
	evas_object_color_set(bg, 0, 0, 0, 255);
	evas_object_move(bg, 0, 0);  /* at canvas' origin */
	evas_object_resize(bg, WIDTH, HEIGHT);  /* covers full canvas */
	evas_object_show(bg);
	evas_object_focus_set(bg, EINA_TRUE);
	evas_object_event_callback_add(bg, EVAS_CALLBACK_KEY_DOWN, _on_keydown, NULL);

	/* status line */
	status = evas_object_text_add(evas);
	evas_object_color_set(status, WHITE);
	evas_object_text_font_source_set(status, FONT_EET);
	evas_object_text_font_set(status, FONT, 10);
	evas_object_move(status, 10, 220);
	evas_object_anti_alias_set(status, EINA_FALSE);
	evas_object_pass_events_set(status, EINA_TRUE);
	evas_object_show(status);

	display_init(evas);
	record_init(evas);
	menu_init(evas);
	module_init();
	ball_init(evas);

	evas_object_lower(bg);

	if (pthread_create(&acq_thread, NULL, acquisition_thread, NULL) != 0) {
		perror("Cannot start acquisition thread");
		ecore_evas_shutdown();
		return EXIT_FAILURE;
	}

	if (pthread_create(&dis_thread, NULL, display_thread, NULL) != 0) {
		perror("Cannot start display thread");
		ecore_evas_shutdown();
		return EXIT_FAILURE;
	}

	cpu_usage = ecore_timer_add(1.0, _on_cpu_timer, NULL);
	_on_cpu_timer(NULL);

	ecore_main_loop_begin();

	ecore_evas_free(ee);
	ecore_evas_shutdown();

	return 0;
}
