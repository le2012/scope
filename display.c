#include <Ecore.h>
#include <Ecore_Evas.h>

#include <stdint.h>
#include <sys/time.h>

#include "scope.h"
#include "curve.h"
#include "display.h"
#include "acq.h"
#include "module.h"
#include "record.h"

#define DIS_HZ 5

static pthread_mutex_t display_mutex;
static pthread_mutex_t trigger_mutex;
static pthread_cond_t trigger_cond;

static Evas_Object *curves[CHANNELS];
static Evas_Object *xline[5];
static Evas_Object *yline[5];
static Evas_Object *tline[2];
static Evas_Object *trigger_normal;
static Evas_Object *trigger_single;
static Evas_Object *auto_text;

static char auto_str[] = "AUTO";

int trigger_chan = 2;
int trigger_sense = 1;
int trigger_ypos = 25;
int trigger_xpos = 75;
int trigger_mode = 0;
int triggered;

float tdiv = 0.001;

volatile int display_refresh;
static Eina_Bool _on_display_timer(void *data);

static inline uint32_t
color2uint32(int r, int g, int b, int a)
{
	return (a << 24) | (r << 16) | (g << 8) | (b << 0);
}

static float
value_to_volt(int16_t v)
{
	return min_voltage + (max_voltage - min_voltage) * v / (1 << bits_per_sample);
}

static int
value_to_y(Channel *ch, int16_t v)
{
	float t = value_to_volt(v);
	float a = 0.5 +  25 * t / ch->vdiv;
	if (a > INT_MAX)
		return INT_MAX;
	else if (a < INT_MIN)
		return INT_MIN;
	return a;
}

static Eina_Bool
_on_x_timer(void *data)
{
	Evas *e = data;
	Evas_Coord x;
	if (!evas_pointer_button_down_mask_get(e))
		return EINA_FALSE;
	evas_pointer_canvas_xy_get(e, &x, NULL);
	trigger_xpos = x - 6;
	if (trigger_xpos > 249) trigger_xpos = 249;
	if (trigger_xpos < 0) trigger_xpos = 0;
	display_update();
	return EINA_TRUE;
}

static Eina_Bool
_on_y_timer(void *data)
{
	Evas *e = data;
	Evas_Coord y;
	if (!evas_pointer_button_down_mask_get(e))
		return EINA_FALSE;
	evas_pointer_canvas_xy_get(e, NULL, &y);
	trigger_ypos = 134 - y;
	if (trigger_ypos > 99) trigger_ypos = 99;
	if (trigger_xpos < -99) trigger_ypos = -99;
	display_update();
	ecore_event_add(REFRESH_MENU, NULL, NULL, NULL);
	return EINA_TRUE;
}

static void
_on_tx_mousedown(void *data, Evas *e, Evas_Object *o, void *einfo)
{
	ecore_timer_add(.1, _on_x_timer, e);
}

static void
_on_ty_mousedown(void *data, Evas *e, Evas_Object *o, void *einfo)
{
	ecore_timer_add(.1, _on_y_timer, e);
}

static void
_on_bg_mousedown(void *data, Evas *e, Evas_Object *o, void *einfo)
{
	int i;
	unsigned int lowest = UINT_MAX, distance;
	int selected = CHANNELS;
	Evas_Coord x, y;
	evas_pointer_canvas_xy_get(e, &x, &y);

	for (i = 0 ; i < CHANNELS ; ++i) {
		distance = curve_distance_get(curves[i], x, y);
		if (distance < lowest) {
			selected = i;
			lowest = distance;
		}
	}

	distance = x - (trigger_xpos + 6);
	distance *= distance;
	if (distance < lowest) {
		selected = -1;
		lowest = distance;
	}

	distance = y - (134 - trigger_ypos);
	distance *= distance;
	if (distance < lowest) {
		selected = -2;
		lowest = distance;
	}

	if (selected == CHANNELS) printf("oops\n");
	else if (selected >= 0) curve_mousedown(curves[selected], x, y);
	else if (selected == -1) ecore_timer_add(.1, _on_x_timer, e);
	else if (selected == -2) ecore_timer_add(.1, _on_y_timer, e);
}

int
display_trigger_ypos_get(void)
{
	return curve_ypos_get(curves[trigger_chan]);
}

static void
_on_trigger_normal_mousedown(void *data, Evas *e, Evas_Object *o, void *einfo)
{
	trigger_mode = 0;
	evas_object_color_set(trigger_normal, RED);
	evas_object_color_set(trigger_single, GRAY);
	triggered = 0;
}

static void
_on_trigger_single_mousedown(void *data, Evas *e, Evas_Object *o, void *einfo)
{
	trigger_mode = 1;
	evas_object_color_set(trigger_normal, GRAY);
	evas_object_color_set(trigger_single, RED);
	triggered = 0;
	if (pthread_mutex_trylock(&trigger_mutex) == 0) {
		pthread_cond_signal(&trigger_cond);
		pthread_mutex_unlock(&trigger_mutex);
	}
}

void
_auto_update_display(void *data)
{
	ecore_event_add(REFRESH_MENU, NULL, NULL, NULL);
	display_update();
}

static void *
auto_thread(void *arg)
{
	int i, j, h, n;
	int wpos;
	int samples = acq_freq / 10;
	int *val;
	int trigf = acq_freq;
	struct sched_param sp;

#ifdef USE_PRIO
	/* Set record thread realtime priority */
	sp.sched_priority = AUTO_THREAD_PRIO;
	pthread_setschedparam(pthread_self(), SCHED_RR, &sp);
#endif

	val = malloc(sizeof *val * samples);
	if (val == NULL) {
		printf("Not enough memory to perform auto calibration\n");
		return NULL;
	}

	for (i = 0 ; i < ACQ_CHANNELS ; ++i) {
		Channel *ch = chan + i;
		int vmin = INT_MAX;
		int vmax = INT_MIN;
		int mean = 0;
		float vcc, vdiv;
		int ypos = -75 + 50 * i;
		int64_t c0 = 0;
		int ac_ndc = 1;
		int freq = 0;
		int above = 1;
		float ampl;
		int percent = i * 25;
		int64_t cxmax;
		int64_t cxhmax;;
		int period = INT_MAX;

		sprintf(auto_str, "%2d %%", percent);

		wpos = (acq_wpos - samples) % ACQ_SAMPLES;
		for (n = 0 ; n < samples ; ++n) {
			val[n] = acq_samples[ACQ_CHANNELS * ((wpos + n) % ACQ_SAMPLES) + i] & 0x3ff;
			mean += val[n];
		}
		mean /= samples;

		for (n = 0 ; n < samples ; ++n) {
			int v = val[n];
			if (v > vmax) vmax = v;
			if (v < vmin) vmin = v;
			c0 += (v - mean) * (v - mean) / samples;
		}
		vcc = value_to_volt(vmax - vmin + 1);

		c0 = (c0 * 3) / 4;
		for (h = 0 ; h < samples / 2 ; h += 2) {
			int64_t cx = 0;

			sprintf(auto_str, "%2d %%", percent + h * 24 / samples / 2);

			for (n = 0; n < samples - h; ++n) {
				int v0 = val[n] - mean;
				int v1 = val[n + h] - mean;
				cx += v0 * v1 / (samples - h);
			}
			if (above && cx < c0) {
				above = 0;
				if (freq) period = cxhmax / freq;
			} else if (!above && cx > c0) {
				above = 1;
				cxmax = cx;
				cxhmax = h;
				++freq;
			} else if (above) {
				if (cx > cxmax) cxhmax = h;
			}
		}

		ac_ndc = freq > 1;
		if (ac_ndc) {
			// signal is alternative, periodic
			ampl = vcc;
		} else {
			if (c0 < 100) {
				// small noise
				ampl = value_to_volt(mean >= 0 ? mean : -mean);
				ypos += mean >= 0 ? -25 : 25;
			} else if (vmin >= 0) {
				// signal is positive
				ampl = value_to_volt(vmax);
				ypos -= 25;
			} else if (vmax <= 0) {
				// signal is negative
				ampl = value_to_volt(-vmin);
				ypos += 25;
			} else {
				// signal is random, alternative
				ampl = vcc;
			}
		}

		for (j = sizeof(calib_v)/sizeof(*calib_v) - 1 ; j > 0 ; --j) {
			if (ampl <= 2.5 * calib_v[j]) break;
		}
		vdiv = calib_v[j];

		printf("ADC%d: Mean %f, Vcc %f V, c0 %lld V2, f %d Hz\n", i + 1, value_to_volt(mean), vcc, c0, acq_freq / period);
		pthread_mutex_lock(&display_mutex);
		ch->ac_ndc = ac_ndc;
		ch->vdiv = vdiv;
		curve_ypos_set(curves[i], ypos);

		if (ac_ndc && freq < trigf) {
			float p = period * 1.0 / acq_freq;
			for (j = sizeof(calib_t)/sizeof(*calib_t) - 1 ; j > 0 ; --j) {
				if (p <= 6 * calib_t[j]) break;
			}
			tdiv = calib_t[j];
			trigger_chan = i;
			trigger_ypos = ypos;
			trigf = freq;
		}
		pthread_mutex_unlock(&display_mutex);

		ecore_main_loop_thread_safe_call_async(_auto_update_display, NULL);
	}

	pthread_mutex_lock(&display_mutex);
	if (trigf == acq_freq) {
		tdiv = 0.005;
		trigger_chan = 0;
		trigger_ypos = 0;
	}
	pthread_mutex_unlock(&display_mutex);

	free(val);

	strcpy(auto_str, "AUTO");

	return NULL;
}

static void
_on_trigger_auto_mousedown(void *data, Evas *e, Evas_Object *o, void *einfo)
{
	pthread_t th;

	if (acq_wpos < acq_freq / 10)
		return;

	if (recording || strcmp(auto_str, "AUTO"))
		return;

	module_select(NULL);
	chan[ACQ_CHANNELS].visible = 0;

	trigger_xpos = 250/2;
	trigger_sense = 1;

	trigger_mode = 0;
	strcpy(auto_str, " 0 %");
	evas_object_text_text_set(auto_text, " 0 %");
	evas_object_color_set(trigger_normal, RED);
	evas_object_color_set(trigger_single, GRAY);

	if (pthread_create(&th, NULL, auto_thread, NULL) != 0) {
		perror("Cannot start auto thread");
		return;
	}
}

/*
 * +---------+
 * |    |    |
 * |    |    |
 * +----+----+
 * |    |    |
 * |    |    |
 * +----+----+
 *
 *
 *
 */
static void
fill_grid(uint32_t *pixels, int stride)
{
	int x, y;

	uint32_t black = color2uint32( BLACK );
	uint32_t darkgray = color2uint32( GRAY );

	stride /= 4;

	for (y = 1 ; y < 200 ; ++y) {
		for (x = 1 ; x < 250 ; ++x) {
			pixels[x + y * stride] = black;
		}
	}

	for (x = 0 ; x <= 250 ; ++x) {
		pixels[x] = darkgray;
		pixels[x + 200 * stride] = darkgray;
		if ((x % 25) == 0) {
			for (y = 0 ; y <= 200 ; y += 5) {
				pixels[x + y * stride] = darkgray;
			}
		}
	}
	for (y = 0 ; y <= 200 ; ++y) {
		pixels[y * stride] = darkgray;
		pixels[250 + y * stride] = darkgray;
		if ((y % 25) == 0) {
			for (x = 0 ; x <= 250 ; x += 5) {
				pixels[x + y * stride] = darkgray;
			}
		}
	}
}

void
display_init(Evas *e)
{
	Evas_Object *bg;
	Evas_Object *o;
	void *pixels;
	int x;
	int i;

	bg = evas_object_image_filled_add(e);
	evas_object_lower(bg);
	evas_object_move(bg, 5, 34);
	evas_object_resize(bg, 251, 201);
	evas_object_image_size_set(bg, 251, 201);
	pixels = evas_object_image_data_convert(bg, EVAS_COLORSPACE_ARGB8888);
	if (pixels == NULL)
		pixels = evas_object_image_data_get(bg, EINA_TRUE);
	fill_grid(pixels, evas_object_image_stride_get(bg));
	evas_object_image_pixels_dirty_set(bg, EINA_TRUE);
	evas_object_show(bg);
	evas_object_event_callback_add(bg, EVAS_CALLBACK_MOUSE_DOWN, _on_bg_mousedown, NULL);

	tline[0] = evas_object_line_add(e);
	evas_object_event_callback_add(tline[0], EVAS_CALLBACK_MOUSE_DOWN, _on_tx_mousedown, NULL);
	evas_object_color_set(tline[0], WHITE);
	evas_object_show(tline[0]);
	tline[1] = evas_object_line_add(e);
	evas_object_event_callback_add(tline[1], EVAS_CALLBACK_MOUSE_DOWN, _on_ty_mousedown, NULL);
	evas_object_color_set(tline[1], WHITE);
	evas_object_show(tline[1]);

	for (i = 0 ; i < CHANNELS ; ++i) {
		Evas_Object *c = curve_add(e);
		curve_ypos_set(c, i < ACQ_CHANNELS ? -100 + 50 * i : 0);
		evas_object_color_set(c, colors[i].r, colors[i].g, colors[i].b, colors[i].a);
		curves[i] = c;
	}

	for (i = 0 ; i < 5 ; ++i) {
		xline[i] = evas_object_line_add(e);
		evas_object_show(xline[i]);
		evas_object_color_set(xline[i], WHITE);
		evas_object_event_callback_add(xline[i], EVAS_CALLBACK_MOUSE_DOWN, _on_tx_mousedown, NULL);

		yline[i] = evas_object_line_add(e);
		evas_object_show(yline[i]);
		evas_object_color_set(yline[i], WHITE);
		evas_object_event_callback_add(yline[i], EVAS_CALLBACK_MOUSE_DOWN, _on_ty_mousedown, NULL);
	}

	// Trigger button
	x = 128;

	o = evas_object_rectangle_add(e);
	evas_object_move(o, x, 0);
	evas_object_resize(o, 45, 32);
	evas_object_color_set(o, BLUE);
	evas_object_show(o);

	o = evas_object_text_add(e);
	evas_object_color_set(o, WHITE);
	evas_object_text_text_set(o, "Trigger");
	evas_object_text_font_source_set(o, FONT_EET);
	evas_object_text_font_set(o, FONT ":style=Bold", 10);
	evas_object_move(o, x, 0);
	evas_object_anti_alias_set(o, EINA_FALSE);
	evas_object_show(o);

	o = evas_object_rectangle_add(e);
	evas_object_move(o, x+3, 12);
	evas_object_resize(o, 18, 18);
	evas_object_color_set(o, GRAY);
	evas_object_show(o);
	evas_object_event_callback_add(o, EVAS_CALLBACK_MOUSE_DOWN, _on_trigger_normal_mousedown, NULL);
	trigger_normal = o;

	o = evas_object_rectangle_add(e);
	evas_object_move(o, x+24, 12);
	evas_object_resize(o, 18, 18);
	evas_object_color_set(o, GRAY);
	evas_object_show(o);
	evas_object_event_callback_add(o, EVAS_CALLBACK_MOUSE_DOWN, _on_trigger_single_mousedown, NULL);
	trigger_single = o;

	o = evas_object_text_add(e);
	evas_object_color_set(o, BLACK);
	evas_object_text_text_set(o, "N");
	evas_object_text_font_source_set(o, FONT_EET);
	evas_object_text_font_set(o, FONT ":style=Bold", 10);
	evas_object_move(o, x+8, 16);
	evas_object_anti_alias_set(o, EINA_FALSE);
	evas_object_pass_events_set(o, EINA_TRUE);
	evas_object_show(o);

	o = evas_object_text_add(e);
	evas_object_color_set(o, BLACK);
	evas_object_text_text_set(o, "S");
	evas_object_text_font_source_set(o, FONT_EET);
	evas_object_text_font_set(o, FONT ":style=Bold", 10);
	evas_object_move(o, x+29, 16);
	evas_object_anti_alias_set(o, EINA_FALSE);
	evas_object_pass_events_set(o, EINA_TRUE);
	evas_object_show(o);

	// Auto display
	x = 81;

	o = evas_object_rectangle_add(e);
	evas_object_move(o, x, 0);
	evas_object_resize(o, 45, 32);
	evas_object_color_set(o, MAGENTA);
	evas_object_show(o);

	o = evas_object_text_add(e);
	evas_object_color_set(o, WHITE);
	evas_object_text_text_set(o, "Display");
	evas_object_text_font_source_set(o, FONT_EET);
	evas_object_text_font_set(o, FONT ":style=Bold", 10);
	evas_object_move(o, x+1, 0);
	evas_object_anti_alias_set(o, EINA_FALSE);
	evas_object_show(o);

	o = evas_object_rectangle_add(e);
	evas_object_move(o, x+3, 12);
	evas_object_resize(o, 39, 18);
	evas_object_color_set(o, GRAY);
	evas_object_show(o);
	evas_object_event_callback_add(o, EVAS_CALLBACK_MOUSE_DOWN, _on_trigger_auto_mousedown, NULL);

	o = evas_object_text_add(e);
	evas_object_color_set(o, BLACK);
	evas_object_text_text_set(o, "AUTO");
	evas_object_text_font_source_set(o, FONT_EET);
	evas_object_text_font_set(o, FONT ":style=Bold", 10);
	evas_object_move(o, x + 10, 16);
	evas_object_anti_alias_set(o, EINA_FALSE);
	evas_object_pass_events_set(o, EINA_TRUE);
	evas_object_show(o);
	auto_text = o;

	pthread_mutex_init(&display_mutex, NULL);
	pthread_mutex_init(&trigger_mutex, NULL);
	pthread_cond_init(&trigger_cond, NULL);

	ecore_timer_add(.2, _on_display_timer, e);
}

static int y_min[CHANNELS][SAMPLES_PER_FRAME*2];
static int y_max[CHANNELS][SAMPLES_PER_FRAME*2];
static int visible[CHANNELS];
static int fps;

static struct plugin_signal signal_arg[CHANNELS];
static uint16_t *volts[CHANNELS];
int result_int;

int
display_fps_get_reset(void)
{
	int f;
	pthread_mutex_lock(&display_mutex);
	f = fps;
	fps = 0;
	pthread_mutex_unlock(&display_mutex);
	return f;
}

static Eina_Bool
_on_display_timer(void *data)
{
	int i;

	evas_object_text_text_set(auto_text, auto_str);

	/* If display thread is waiting for a go, give it */
	if (trigger_mode == 0 && !pthread_mutex_trylock(&trigger_mutex)) {
		pthread_cond_signal(&trigger_cond);
		pthread_mutex_unlock(&trigger_mutex);
	}

	if (!display_refresh) return EINA_TRUE;
	display_refresh = 0;

	pthread_mutex_lock(&display_mutex);
	++fps;

	Evas_Object *trig = trigger_mode ? trigger_single : trigger_normal;
	if (triggered) {
		evas_object_color_set(trig, GREEN);
	} else {
		evas_object_color_set(trig, RED);
	}

	ecore_event_add(REFRESH_MODULE_VALUE, NULL, NULL, NULL);
	for (i = 0 ; i < CHANNELS ; ++i) {
		if (visible[i] && (i < ACQ_CHANNELS || (plugin && plugin->plugin_ops.v1->type == PLUGIN_RET_SIGNAL))) {
			curve_update(curves[i], y_min[i], y_max[i]);
			evas_object_show(curves[i]);
		} else {
			evas_object_hide(curves[i]);
		}
	}

	pthread_mutex_unlock(&display_mutex);

	int x = 5 + trigger_xpos;
	int y = 134 - trigger_ypos;

	evas_object_line_xy_set(xline[4], x - 0, 235, x + 0, 235);
	evas_object_line_xy_set(xline[3], x - 1, 236, x + 1, 236);
	evas_object_line_xy_set(xline[2], x - 2, 237, x + 2, 237);
	evas_object_line_xy_set(xline[1], x - 3, 238, x + 3, 238);
	evas_object_line_xy_set(xline[0], x - 4, 239, x + 4, 239);

	evas_object_line_xy_set(yline[4], 0, y - 4, 0, y + 4);
	evas_object_line_xy_set(yline[3], 1, y - 3, 1, y + 3);
	evas_object_line_xy_set(yline[2], 2, y - 2, 2, y + 2);
	evas_object_line_xy_set(yline[1], 3, y - 1, 3, y + 1);
	evas_object_line_xy_set(yline[0], 4, y - 0, 4, y + 0);

	evas_object_line_xy_set(tline[0], x, 35, x, 233);
	evas_object_line_xy_set(tline[1], 6, y, 254, y);

	return EINA_TRUE;
}

static acq_countdown display_countdown;

void
display_at(uint64_t wpos64, int trig)
{
	unsigned int freq = acq_freq;
	unsigned int spf = SAMPLES_PER_FRAME  * tdiv * freq / 25 + 2;
	int n, x, xx;
	unsigned int i;
	unsigned int wpos;
	int mean[CHANNELS];
	int ret;

	wpos = (wpos64 - 5 * spf) % ACQ_SAMPLES;
	for (i = 0 ; i < ACQ_CHANNELS ; ++i) {
		if (!chan[i].ac_ndc) continue;
		mean[i] = 0;
		for (n = 0 ; n < 5 * spf ; ++n) {
			mean[i] += acq_samples[ACQ_CHANNELS * ((wpos + n) % ACQ_SAMPLES) + i] & 0x3ff;
		}
		mean[i] /= 5 * (int) spf;
	}


	module_get();
	if (plugin) {
		struct plugin_signal_settings settings = {
			.bits_per_sample = bits_per_sample,
			.min_voltage = min_voltage,
			.max_voltage = max_voltage,
			.period = 1.0 / acq_freq,
		};
		plugin->plugin_ops.v1->reset(display_ctxt, &settings);

		for (i = 0 ; i < CHANNELS ; ++i) {
			if (spf > signal_arg[i].nb_elts) {
				volts[i] = realloc(volts[i], spf * sizeof *volts[0]);
				signal_arg[i].nb_elts = spf;
			}
			signal_arg[i].volts = volts[i];
			signal_arg[i].start = 0;
			signal_arg[i].end = spf;
		}

		for (i = 0 ; i < plugin_nb_args_get() ; ++i) {
			for (n = 0 ; n < spf ; ++n) {
				volts[i][n] = acq_samples[ACQ_CHANNELS*((wpos + n) % ACQ_SAMPLES) + plugin_args[i]] & 0x3ff;
				if (chan[plugin_args[i]].ac_ndc)
					volts[i][n] -= mean[plugin_args[i]];
			}
		}

		if (plugin->plugin_ops.v1->type == PLUGIN_RET_INT) {
			ret = plugin->plugin_ops.v1->process.ret_int(display_ctxt, signal_arg, &result_int);
		} else {
			ret = plugin->plugin_ops.v1->process.ret_signal(display_ctxt, signal_arg, &signal_arg[ACQ_CHANNELS]);
			if (chan[ACQ_CHANNELS].ac_ndc) {
				mean[ACQ_CHANNELS] = 0;
				for (n = 0 ; n < spf ; ++n)
					mean[ACQ_CHANNELS] += volts[ACQ_CHANNELS][n];
				mean[ACQ_CHANNELS] /= spf;
			}
		}

		if (ret) {
			DEBUG("module result %d, hiding.\n", ret);
			chan[ACQ_CHANNELS].visible = 0;
		}
	}
	module_put();

	pthread_mutex_lock(&display_mutex);

	/* Clear output buffer */
	for (i = 0 ; i < CHANNELS ; ++i) {
		visible[i] = chan[i].visible;
		for (n = 0 ; n < 2*SAMPLES_PER_FRAME ; ++n) {
			y_min[i][n] = INT_MAX;
			y_max[i][n] = INT_MIN;
		}
	}

	wpos = wpos64 % ACQ_SAMPLES;
	float scale = 25.0 / (tdiv * freq);
	for (n = 0, xx = 0 ; n < spf ; ++n) {
		x = n * scale;

		for (i = 0 ; i < CHANNELS ; ++i) {
			int16_t v;
			int y;
			if (i < ACQ_CHANNELS) {
				v = acq_samples[ACQ_CHANNELS * wpos + i] & 0x3ff;
			} else if (ret || !plugin || plugin->plugin_ops.v1->type == PLUGIN_RET_INT) {
				break;
			} else {
				v = volts[ACQ_CHANNELS][n];
			}
			if (chan[i].ac_ndc)
				v -= mean[i];
			y = value_to_y(&chan[i], v);;
			if (chan[i].invert) y = -y;
			if (y < y_min[i][x])
				y_min[i][x] = y;
			if (y > y_max[i][x])
				y_max[i][x] = y;
		}

		if (x - xx > 1) {
			int d = x - xx;
			for (i = 0 ; i < CHANNELS ; i++) {
				int dmin = y_min[i][x] - y_min[i][xx];
				int dmax = y_max[i][x] - y_max[i][xx];
				int j;
				for (j = 1 ; j < d ; ++j) {
					y_min[i][xx+j] = y_min[i][xx] + j * dmin / d;
					y_max[i][xx+j] = y_max[i][xx] + j * dmax / d;
				}
			}
		}

		if (x > SAMPLES_PER_FRAME) break;

		xx = x;
		wpos = (wpos + 1) % ACQ_SAMPLES;
	}

	triggered = trig;
	display_update();

	/* should use 3-ways producer/consumer lockless method */
	pthread_mutex_unlock(&display_mutex);
}

void *
display_thread(void *param)
{
	unsigned int freq, spf, xp;
	uint64_t wpos64, last64;
	unsigned int n;
	unsigned int wpos;
	int i;
	unsigned int mean[CHANNELS];
	int trig;
	struct sched_param sp;
	struct timespec ts0, ts;

#ifdef USE_PRIO
	/* Set record thread realtime priority */
	sp.sched_priority = DIS_THREAD_PRIO;
	pthread_setschedparam(pthread_self(), SCHED_RR, &sp);
#endif

	pthread_mutex_lock(&trigger_mutex);

	while (1) {
		if (trigger_mode == 0 || (trigger_mode == 1 && trig == 1)) {
			pthread_cond_wait(&trigger_cond, &trigger_mutex);
		} else {
			usleep(10000);
		}
		//DEBUG("got trigger mode %d\n", trigger_mode);

		freq = acq_freq;
		spf = SAMPLES_PER_FRAME * tdiv * freq / 25 + 2;
		xp = spf * (SAMPLES_PER_FRAME - trigger_xpos) / SAMPLES_PER_FRAME;
		wpos64 = acq_wpos - xp;
		i = trigger_chan;
		trig = 0;

		/* which trigger level in ADC value */
		int ypos = trigger_ypos - curve_ypos_get(curves[i]);
		float yval = ypos * chan[i].vdiv / 25.0;
		int16_t v = ((yval - min_voltage) / (max_voltage - min_voltage)) * (1 << bits_per_sample);
		if (chan[i].ac_ndc) {
			int m = 0;
			wpos = (wpos64 - 5 * spf) % ACQ_SAMPLES;
			for (n = 0 ; n < 5 * spf ; ++n)
				m += acq_samples[ACQ_CHANNELS * (++wpos % ACQ_SAMPLES) + i] & 0x3ff;
			m /= 5 * spf;
			v += m;
			mean[i] = m;
		}
		//DEBUG("ypos %d yval %f trig_v %d\n", ypos, yval, v);

		if (trigger_mode == 0) {
			int side = 0, last_side;
			last64 = wpos64 - freq / DIS_HZ;
			wpos = last64 % ACQ_SAMPLES;
			for (n = freq / DIS_HZ ; n > 0 ; --n) {
				/* position wrt. trigger value */
				last_side = side;
				int y = acq_samples[ACQ_CHANNELS * ((wpos + n) % ACQ_SAMPLES) + i] & 0x3ff;
				if (y > v + 1) side = 1;
				if (y < v - 1) side = -1;
				if ((side + last_side == 0) &&
						((trigger_sense && side == -1) ||
						 (!trigger_sense && side == 1))) {
					trig = 1;
					break;
				}
			}
			if (!trig) n = freq / DIS_HZ;
		} else {
			int side = 0, last_side;
			unsigned int samples = wpos64 - last64;
			wpos = last64 % ACQ_SAMPLES;
			for (n = 0 ; n < wpos64 - last64 ; ++n) {
				/* position wrt. trigger value */
				last_side = side;
				int y = acq_samples[ACQ_CHANNELS * ((wpos + n) % ACQ_SAMPLES) + i] & 0x3ff;
				if (y > v + 1) side = 1;
				if (y < v - 1) side = -1;
				if ((side + last_side == 0) &&
						((trigger_sense && side == 1) ||
						 (!trigger_sense && side == -1))) {
					trig = 1;
					break;
				}
			}
			if (!trig) continue;
		}

		clock_gettime(CLOCK_MONOTONIC, &ts0);
		display_at(last64 + n + xp - spf, trig);
		clock_gettime(CLOCK_MONOTONIC, &ts);

		/* throttle display */
		if (ts.tv_sec -  ts0.tv_sec > 1)
			usleep(1000000);
		else
			usleep((ts.tv_sec - ts0.tv_sec) * 1000000 + (ts.tv_nsec - ts0.tv_nsec) / 1000);

		last64 = wpos64;
	}

	pthread_mutex_unlock(&trigger_mutex);

	return NULL;
}
