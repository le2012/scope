#include <Ecore.h>
#include <Ecore_Evas.h>

#include <stdio.h>
#include <stdarg.h>

#include "scope.h"
#include "curve.h"
#include "display.h"

#define _curve_type "Curve"
#define EVT_CURVE_CLICKED "tab,clicked"

typedef struct _Curve_Data Curve_Data;

/*
 * This structure augments clipped smart object's instance data
 */
struct _Curve_Data
{
	Evas_Object_Smart_Clipped_Data base;
	Evas_Object *vline[249];
	Evas_Object *pline[5];
	int ypos;
};

#define CURVE_DATA_GET(o, ptr) \
  Curve_Data * ptr = evas_object_smart_data_get(o)

#define CURVE_DATA_GET_OR_RETURN(o, ptr)        \
  CURVE_DATA_GET(o, ptr);                       \
  if (!ptr)                                                  \
    {                                                        \
       fprintf(stderr, "No widget data for object %p (%s)!", \
               o, evas_object_type_get(o));                  \
       fflush(stderr);                                       \
       abort();                                              \
       return;                                               \
    }

#define CURVE_DATA_GET_OR_RETURN_VAL(o, ptr, val) \
  CURVE_DATA_GET(o, ptr);                         \
  if (!ptr)                                                    \
    {                                                          \
       fprintf(stderr, "No widget data for object %p (%s)!",   \
               o, evas_object_type_get(o));                    \
       fflush(stderr);                                         \
       abort();                                                \
       return val;                                             \
    }

EVAS_SMART_SUBCLASS_NEW(_curve_type, _curve,
                        Evas_Smart_Class, Evas_Smart_Class,
                        evas_object_smart_clipped_class_get, NULL);

static void
_on_mousemove(void *data, Evas *e, Evas_Object *o, void *einfo)
{
	printf("mouse move !!\n");
}

static Evas_Coord overy;

static Eina_Bool
_on_timer(void *data)
{
	Evas_Object *o = data;
	Evas_Coord y;
	EVAS_SMART_DATA_ALLOC(o, Curve_Data);
	Evas *e = evas_object_evas_get(o);

	if (!evas_pointer_button_down_mask_get(e)) {
		return EINA_FALSE;
	}

	evas_pointer_canvas_xy_get(e, NULL, &y);
	priv->ypos -= y - overy;
	if (priv->ypos > 99) priv->ypos = 99;
	if (priv->ypos < -99) priv->ypos = -99;
	overy = y;
	evas_object_smart_changed(o);
	display_update();
	ecore_event_add(REFRESH_MENU, NULL, NULL, NULL);
	return EINA_TRUE;
}

static void
_on_mousedown(void *data, Evas *e, Evas_Object *o, void *einfo)
{
	evas_pointer_canvas_xy_get(e, NULL, &overy);
	ecore_timer_add(.1, _on_timer, data);
}

/* create and setup a new tab object's internals */
static void
_curve_smart_add(Evas_Object *o)
{
	EVAS_SMART_DATA_ALLOC(o, Curve_Data);
	Evas *e = evas_object_evas_get(o);
	Evas_Object *clipper;
	int i;

	_curve_parent_sc->add(o);

	clipper = evas_object_smart_clipped_clipper_get(o);
	evas_object_move(o, 6, 36);
	evas_object_move(clipper, 0, 36);
	evas_object_resize(clipper, 255, 198);

	for (i = 0 ; i < 249 ; ++i) {
		priv->vline[i] = evas_object_line_add(e);
		evas_object_clip_set(priv->vline[i], clipper);
		evas_object_show(priv->vline[i]);
		evas_object_event_callback_add(priv->vline[i], EVAS_CALLBACK_MOUSE_DOWN, _on_mousedown, o);
	}

	for (i = 0 ; i < 5 ; ++i) {
		priv->pline[i] = evas_object_line_add(e);
		evas_object_clip_set(priv->pline[i], clipper);
		evas_object_show(priv->pline[i]);
		evas_object_event_callback_add(priv->pline[i], EVAS_CALLBACK_MOUSE_DOWN, _on_mousedown, o);
	}
}

static void
_curve_smart_del(Evas_Object *o)
{
	CURVE_DATA_GET(o, priv);
	int i;

	for (i = 0 ; i < 249 ; ++i)
		evas_object_del(priv->vline[i]);

	_curve_parent_sc->del(o);
}

/* act on child objects' properties, before rendering */
static void
_curve_smart_calculate(Evas_Object *o)
{
}

static void
_curve_smart_color_set(Evas_Object *o, int r, int g, int b, int a)
{
	CURVE_DATA_GET(o, priv);
	int i;

	for (i = 0 ; i < 249 ; ++i)
		evas_object_color_set(priv->vline[i], r, g, b, a);

	for (i = 0 ; i < 5 ; ++i)
		evas_object_color_set(priv->pline[i], r, g, b, a);
}

/* setting our smart interface */
static void
_curve_smart_set_user(Evas_Smart_Class *sc)
{
	/* specializing these */
	sc->add = _curve_smart_add;
	sc->del = _curve_smart_del;
	sc->color_set = _curve_smart_color_set;

	/* clipped smart object has no hook on resizes or calculations */
	sc->calculate = _curve_smart_calculate;
}

/* BEGINS example smart object's own interface */

/* add a new example smart object to a canvas */
Evas_Object *
curve_add(Evas *evas)
{
	return evas_object_smart_add(evas, _curve_smart_class_new());
}

void
curve_update(Evas_Object *o, int y_min[], int y_max[])
{
	Evas_Coord x, y, w, h;
	int i;

	CURVE_DATA_GET_OR_RETURN(o, priv);

	evas_object_geometry_get(o, &x, &y, NULL, NULL);
	y = y + 98 - priv->ypos;

	evas_object_line_xy_set(priv->vline[0], x, y - y_min[0], x, y - y_max[0]);
	x++;
	for (i = 1 ; i < SAMPLES_PER_FRAME ; ++i, ++x) {
		int y1 = y_min[i-1] < y_min[i] ? y_min[i-1] : y_min[i];
		int y2 = y_max[i-1] > y_max[i] ? y_max[i-1] : y_max[i];
		if (y1 == y2) y2 = y1 + 1;
		evas_object_line_xy_set(priv->vline[i], x, y - y1, x, y - y2);
	}

	evas_object_line_xy_set(priv->pline[0], 0, y - 4, 0, y + 4);
	evas_object_line_xy_set(priv->pline[1], 1, y - 3, 1, y + 3);
	evas_object_line_xy_set(priv->pline[2], 2, y - 2, 2, y + 2);
	evas_object_line_xy_set(priv->pline[3], 3, y - 1, 3, y + 1);
	evas_object_line_xy_set(priv->pline[4], 4, y - 0, 4, y + 0);
}

void
curve_mousedown(Evas_Object *o, Evas_Coord x, Evas_Coord y)
{
	overy = y;
	ecore_timer_add(.1, _on_timer, o);
}

unsigned int
curve_distance_get(Evas_Object *o, Evas_Coord x, Evas_Coord y)
{
	unsigned int lowest = UINT_MAX, d;
	int i;
	CURVE_DATA_GET_OR_RETURN(o, priv);

	if (!evas_object_visible_get(o)) return lowest;

	for (i = 0 ; i < SAMPLES_PER_FRAME ; ++i) {
		Evas_Coord ox, oy, oh;
		int j;
		evas_object_geometry_get(priv->vline[i], &ox, &oy, NULL, &oh);
		for (j = oy ; j < oy + oh ; ++j) {
			unsigned int dx = x - ox;
			unsigned int dy = y - j;
			d = dx * dx + dy * dy;
			if (d < lowest) lowest = d;
		}
	}

	return lowest;
}

int
curve_ypos_get(Evas_Object *o)
{
	CURVE_DATA_GET_OR_RETURN(o, priv);
	return priv->ypos;
}

void
curve_ypos_set(Evas_Object *o, int ypos)
{
	CURVE_DATA_GET_OR_RETURN(o, priv);
	priv->ypos = ypos;
	evas_object_smart_changed(o);
}
