
extern int REFRESH_MENU;
extern int REFRESH_MODULE_VALUE;

//#define FONT "DejaVu Sans Mono"
#define FONT_EET "/usr/share/fonts/font.eet"
#define FONT "DejaVuSansMono"

#define CHANNELS 5

typedef struct {
	int r, g, b, a;
} color;

extern color colors[];

// VGA COLORS
#define BLACK		0,0,0,255
#define RED		170,0,0,255
#define GREEN		0,170,0,255
#define YELLOW		170,85,0,255
#define BLUE		0,0,170,255
#define MAGENTA		170,0,170,255
#define CYAN		0,170,170,255
#define	GRAY		170,170,170,255
#define DARKGRAY	85,85,85,255
#define L_RED		255,85,85,255
#define	L_GREEN		85,255,85,255
#define L_YELLOW	255,255,85,255
#define L_BLUE		85,85,255,255
#define L_MAGENTA	255,85,255,255
#define L_CYAN		85,255,255,255
#define WHITE		255,255,255,255

#define SAMPLES_PER_FRAME 249

typedef struct {
	char name[8];
	float vdiv;
	int invert;
	int y_min[SAMPLES_PER_FRAME];
	int y_max[SAMPLES_PER_FRAME];
	int visible;
	int ac_ndc;
} Channel;

extern Channel chan[CHANNELS];

static float calib_v[] = { 5.0, 2.0, 1.0, 0.5, 0.2, 0.1, 0.05, 0.02 }; //, 0.01 };
static float calib_t[] = {
	0.05, 0.02, 0.01,
	0.005, 0.002, 0.001,
	0.0005, 0.0002, 0.0001,
	0.00005, 0.00002, 0.00001
};

extern float tdiv;
extern int trigger_chan;
extern int trigger_sense;
extern int trigger_ypos;
extern int trigger_xpos;
extern volatile int triggered_at;

extern float min_voltage;
extern float max_voltage;
extern int bits_per_sample;

#define USE_PRIO
#define FLUSH_THREAD_PRIO 95
#define ACQ_THREAD_PRIO 90
#define REC_THREAD_PRIO 80
#define DEFAULT_THREAD_PRIO 50
#define DIS_THREAD_PRIO 40
#define BALL_THREAD_PRIO 35
#define AUTO_THREAD_PRIO 30


#define DEBUG(format, args...) do { \
	struct timespec ts; \
	clock_gettime(CLOCK_MONOTONIC, &ts); \
	printf("[%d.%03d] %s:%d " format, ts.tv_sec, ts.tv_nsec / 1000000,__func__,__LINE__, args); \
	} while(0)
